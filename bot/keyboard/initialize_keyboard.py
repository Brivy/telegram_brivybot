"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


initialize_keyboard.py
---------------
This file is to initialize keyboards for
different menu's
To build this keyboards, the file uses "replyKeyboard.py"
"""

from bot.keyboard import ReplyKeyboard
import logging

def start_menu_keyboard():
    """
    Menu that shows on startup
    Mainly used for debugging

    :return: A JSON format of the keyboard object
    """
    start_button = ["/start"]
    keyboard = ReplyKeyboard.ReplyKeyboard([start_button])
    logging.debug("Building start menu")
    return keyboard.dump_to_json()


def bot_menu_keyboard(root_check):
    """
    Main menu keyboard
    Shows basic commands to the user while showing advanced
    commands to the root user

    :return: A JSON format of the keyboard object
    """
    first_options = ["/getAllLights", "/getAllWeather"]
    second_options = ["/getAllTemp", "/getAllUtility"]
    help_button = ["/help"]
    if root_check:
        settings = ["/settings"]
        keyboard = ReplyKeyboard.ReplyKeyboard([first_options, second_options, settings, help_button])
    else:
        keyboard = ReplyKeyboard.ReplyKeyboard([first_options, second_options, help_button])
    logging.debug("Building menu keyboard")
    return keyboard.dump_to_json()


def display_users_menu():
    """
    Keyboard that is used to show all the different user levels

    :return: A JSON format of the keyboard object
    """
    users_all_root = ["/users all", "/users root"]
    users_users_block = ["/users user", "/users block"]
    return_button = ["Return to the settings menu"]
    keyboard = ReplyKeyboard.ReplyKeyboard([users_all_root, users_users_block, return_button])
    logging.debug("Building display users menu")
    return keyboard.dump_to_json()


def device_menu(idx, device_type, added=False):
    """
    When you search for a device, show a menu with different
    device options
    These options differ from the state of the device

    :param idx: The ID of the device
    :param device_type: The type of device
    :param added: Is the device added to domoticz?
    :return: A JSON format of the keyboard object
    """
    return_button = ["Return to the devices menu"]
    if added:
        add_rem_button = ["Remove device " + idx + " from domoticz?"]
        test_button = ["Test used device " + idx + " (" + device_type + ")"]
        rename_button = ["Rename device " + idx]
        keyboard = ReplyKeyboard.ReplyKeyboard([add_rem_button, rename_button, test_button, return_button])
    else:
        add_rem_button = ["Add device " + idx + " to domoticz?"]
        test_button = ["Test unused device " + idx + " (" + device_type + ")"]
        keyboard = ReplyKeyboard.ReplyKeyboard([add_rem_button, test_button, return_button])

    logging.debug("Building device menu")
    return keyboard.dump_to_json()


def all_lights_menu(lights_array):
    """
    Keyboard that is used to show all available lights
    If a keyboard is not used, don't show him within this collection

    :param lights_array: Array that holds all information about the lights
    :return: A JSON format of the keyboard object
    """
    light_list = []
    for lights in lights_array:
        idx = lights["idx"]
        device_name = lights["Name"]
        data = lights["Data"]
        used = lights["Used"]

        if used:
            if data == "On":
                string = ["Light " + idx + " - " + device_name + " (On -> Off)"]
            else:
                string = ["Light " + idx + " - " + device_name + " (Off -> On)"]
            light_list.append(string)

    light_list.append(["Return"])
    keyboard = ReplyKeyboard.ReplyKeyboard(light_list)
    logging.debug("Building all lights menu")
    return keyboard.dump_to_json()


def all_weather_menu(meter_array):
    """
    Keyboard that is used to show all available temperature and humidity meters
    If an unknown device is encountered, skip it

    :param meter_array: Array that holds all information about the meters
    :return: A JSON format of the keyboard object
    """
    meter_list = []
    for meter in meter_array:
        idx = meter["idx"]
        description = meter["Name"]

        if description == "Unknown":
            continue

        string = ["Meter " + idx + " - " + description]
        meter_list.append(string)

    meter_list.append(["Return"])
    keyboard = ReplyKeyboard.ReplyKeyboard(meter_list)
    logging.debug("Building all weather menu")
    return keyboard.dump_to_json()


def general_data_option_menu():
    """
    Keyboard that shows all the main data.json options

    :return: A JSON format of the keyboard object
    """
    tbot_token_button = ["The telegram token"]
    username_button = ["The domoticz username"]
    password_button = ["The domoticz password"]
    ip_button = ["The domoticz ip"]
    port_button = ["The domoticz port"]
    return_button = ["Return to the settings menu"]
    keyboard = ReplyKeyboard.ReplyKeyboard([tbot_token_button, username_button,
                                            password_button, ip_button, port_button, return_button])
    logging.debug("Building general data option menu")
    return keyboard.dump_to_json()


def edit_general_data_menu(data_type, data_object):
    """
    Keyboard that shows the options for a data device within the data.json file

    :param data_object: The data object from the data.json file
    :return: A JSON format of the keyboard object
    """
    edit_button = ["Change the " + data_type + " " + data_object + " object"]
    return_button = ["Return to the data options menu"]
    keyboard = ReplyKeyboard.ReplyKeyboard([edit_button, return_button])
    logging.debug("Building edit general data menu")
    return keyboard.dump_to_json()


def device_option_menu():
    """
    Keyboard that shows all the options that can show devices

    :return: A JSON format of the keyboard object
    """
    device_used_all = ["/device all", "/device used"]
    device_unused = ["/device not used", "Return to the settings menu"]
    return_button = ["Return to the main menu"]
    keyboard = ReplyKeyboard.ReplyKeyboard([device_used_all, device_unused, return_button])
    logging.debug("Building device option menu")
    return keyboard.dump_to_json()


def settings_menu():
    """
    Keyboard that shows all the options that can be configured

    :return: A JSON format of the keyboard
    """
    device_user_button = ["/device", "/users"]
    data_button = ["/edit_data", "/log"]
    return_button = ["Return to main menu"]
    keyboard = ReplyKeyboard.ReplyKeyboard([device_user_button, data_button, return_button])
    logging.debug("Building settings menu")
    return keyboard.dump_to_json()


def ordered_device_menu(devices_array):
    """
    Keyboard that is used to show all not used devices by idx and name

    :param devices_array: Array that holds all information about the meters
    :return: A JSON format of the keyboard object
    """
    devices_list = []
    for device in devices_array:
        idx = device["idx"]
        device_type = device["Type"]

        string = ["Device " + idx + " - (" + device_type + ")"]
        devices_list.append(string)

    devices_list.append(["Return to the devices menu"])
    keyboard = ReplyKeyboard.ReplyKeyboard(devices_list)
    logging.debug("Building ordered device menu")
    return keyboard.dump_to_json()


def display_users(users_array, level):
    """
    Keyboard that is used to show all the users under each other
    Different results are shown when different levels are used

    :param users_array: The array containing all the users
    :param level: A user level
    :return: A JSON format of the keyboard object
    """
    user_list = []

    if level == "all":
        for specific_level in users_array:
            users = users_array[specific_level]
            for user_id, user_name in users.items():
                text = ["User: " + user_name + " - " + str(user_id) + " (" + specific_level + ")"]
                user_list.append(text)
    else:
        for user_id, user_name in users_array.items():
            text = ["User: " + user_name + " - " + str(user_id) + " (" + level + ")"]
            user_list.append(text)

    user_list.append(["Return to the users menu"])
    keyboard = ReplyKeyboard.ReplyKeyboard(user_list)
    logging.debug("Building display users menu")
    return keyboard.dump_to_json()


def display_user_options(user_name, user_id, level):
    """
    Keyboard with the different user options
    A different set of options is shown per level

    :param user_name: The name of the user
    :param user_id: The id of the user
    :param level: A user level
    :return: A JSON format of the keyboard object
    """
    add_root = [user_name + " (" + user_id + ") from '" + level + "' to 'root' level?"]
    add_user = [user_name + " (" + user_id + ") from '" + level + "' to 'user' level?"]
    add_block = [user_name + " (" + user_id + ") from '" + level + "' to 'block' level?"]
    return_button = ["Return to the users menu"]

    if level == "root":
        keyboard = ReplyKeyboard.ReplyKeyboard([add_user, add_block, return_button])
    elif level == "user":
        keyboard = ReplyKeyboard.ReplyKeyboard([add_root, add_block, return_button])
    else:
        keyboard = ReplyKeyboard.ReplyKeyboard([add_root, add_user, return_button])
    logging.debug("Building display user options menu")
    return keyboard.dump_to_json()
