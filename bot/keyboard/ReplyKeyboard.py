"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


replyKeyboard.py
---------------
This file builds a reply keyboard from
the given attributes
This object must be converted to json
"""

import json

class ReplyKeyboard:

    def __init__(self, button_array):
        """
        Initialize a reply keyboard
        This button has the following attributes:
            - The keyboard - This exists of an array of keyboard_buttons
            - Resize_keyboard - This attribute will make it possible to
            use the keyboard horizontal or vertical
            - One_time_keyboard - If this argument is True, the keyboard
            will disappear after one use
            - Selective - Used for sending keyboards to specific users
        """

        self.keyboard = button_array
        self.resize_keyboard = True
        self.one_time_keyboard = False
        self.selective = None


    def dump_to_json(self):
        """
        Turn the keyboard to a json format
        This keyboard can be used when using the sendMessage method

        :return: A json format of the keyboard object
        """
        json_keyboard = json.dumps({'keyboard': self.keyboard,
                                    'resize_keyboard': self.resize_keyboard,
                                    'one_time_keyboard': self.one_time_keyboard})
        return json_keyboard
