"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


tbot_message.py
---------------
Used for getting messages of the user
This file also can check on the message type and
if a message has been edited.
"""
import logging
import re


def get_message(tbot, thread_sender=None, new_install=False):
    """
    Function that checks the messages of a user
    If the offset of the telegrambot is still None (only occurs when a
    get_last_update returns None in constructor of the telegrambot)
    Also checks on message_types in case the user doesn't send text messages

    When it comes across a 409 error within the get_updates function,
    return him back to this functions caller

    :param tbot: The telegrambot itself
    :param thread_sender: If there is a active thread, initialize this variable
    ;param new_install: Is always false except when the bot goes through a new install
    :return: Return the received message from the user OR return None
    when an invalid type has been found OR return 409 when a 409 error
    has been found
    """
    while True:
        message_json = tbot.get_updates(new_install, thread_sender, tbot.new_offset, 1800)
        if message_json:
            logging.debug("Valid message found")
            break

    # Check for the multiple instance error
    if "409" in message_json:
        return message_json

    # If the offset is still empty, the tbot initializes it here
    if tbot.new_offset is None:
        logging.info("Offset is emtpy, initialize new offset with get_last_update")
        tbot.new_offset = tbot.get_last_update()

    message_type = get_message_type(message_json)
    tbot.new_offset += 1
    if not message_type:
        return None
    message = message_json[0]["message"][message_type]
    return message


def get_message_type(message_json):
    """
    Get the type of message that has been send to the bot
    This message can exist out of the following type:
        - "text"

    Future types:
        - "sticker"
        - "picture"
        - "voice"
        - "animation"

    :param message_json: The message must be checked on type
    :return: The given message type
    """
    message = message_json[0]["message"]
    message_type = re.search(r'(text)', str(message))
    if message_type:
        return message_type.group()
    logging.warning("Invalid datatype found, abort message")
    return None


def check_edited_message(message):
    """
    Check of a message is edited
    This is because users can send edited messages which have another tag with them
    If this tag is not properly handed, the bot will crash

    Also, there must be a check if the array is empty, else the message check will crash

    :param message: The message that will be checked on edits
    :return: True if the message is an edit and False if the message is not edited
    """
    if message != [] and "edited_message" in message[0]:
        logging.warning("Edited message found, abort message")
        return True
    return False
