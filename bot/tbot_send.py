"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


tbot_send.py
------------
Used for sending messages to the users
This file is also used for sending the intruder messages
and the custom Root user only messages
"""
import logging

import requests
from commands.commands import t_data

def send_message(tbot, message, keyboard=None):
    """
    Send a message to the telegram bot
    The bot gets the sender his id to send the message to the right person

    All commando's that are send outside a command thread will use the standard sender_id
    Otherwise use the sender ID from the thread via the TLS

    :param tbot: the telegrambot itself
    :param message: The string of text that the bot should send to the user
    :param keyboard: A json format of an usable telegram keyboard
    :return: Returns the status code to the user
    """
    try:
        if not hasattr(t_data, "sender_id"):
            params = {'chat_id': tbot.sender_id, 'text': message, 'reply_markup': keyboard}
        else:
            params = {'chat_id': t_data.sender_id, 'text': message, 'reply_markup': keyboard}
        method = 'sendMessage'
        logging.debug("Send message '" + message + "'")
        return requests.post(tbot.api_url + method, params)
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"


def send_message_to_all_users(tbot, message, keyboard=None):
    """
    Sends a message to all registered users (except the blocked ones)

    :param tbot: The telegrambot itself
    :param message: The string of text that the bot should send to the user
    :param keyboard: A json format of an usable telegram keyboard
    :return: Returns the status code to the user
    """
    try:
        for level in tbot.data["userlevels"]:
            if level is "block":
                continue
            for user in tbot.data["userlevels"][level]:
                params = {'chat_id': user, 'text': message, 'reply_markup': keyboard}
                method = 'sendMessage'
                requests.post(tbot.api_url + method, params)
        logging.debug("Send message '" + message + "' to all users")
        return "200"
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"


def send_intruder_message(tbot):
    """
    Sends a message to the root users and the specific user when an unauthorized user
    is trying to access the bot
    When this user is not presented in the block_user_attempts list, send a message to the root

    :param tbot: The telegrambot itself
    :return: Returns the status code to the user
    """
    message = ("There has been an attempt to control the bot by: " +
               tbot.sender_name + " and ID:  " + str(tbot.sender_id))
    check_user_block_list(tbot, message)
    logging.debug("Send intruder a no access allowed message")
    return send_message(tbot, "No access allowed")


def send_new_user_message(tbot):
    """
    Send a message to a new user and notify the root of this user
    The user will than be added to the block list and the block_user_attempts list
    so that the root won't be spammed as much

    :param tbot: The telegrambot itself
    :return: Returns the status cod to the user
    """
    message = ("Hey " + tbot.sender_name + "!\n"
               "The root user has been notified and you will receive a message "
               "when you can enter the bot\n"
               "For now, just wait patiently until you are notified")
    message_root = ("A new user tries to register:\n"
                    "Name: " + tbot.sender_name + "\n"
                    "ID: " + str(tbot.sender_id))
    check_user_block_list(tbot, message_root)
    logging.debug("Send new user a new user message")
    return send_message(tbot, message)


def check_user_block_list(tbot, message):
    """
    Check if the user is already in the block_user_attempts list
    When already inside, the bot will not send an extra message to the root user

    :param tbot: The telegrambot itself
    :param message: The string of text that the bot should send to the user
    :return: Returns the status code to the user
    """
    if tbot.block_user_attempts:
        for user in tbot.block_user_attempts:
            if user != tbot.sender_name:
                contact_root_message(tbot, message)
                tbot.block_user_attempts.append(tbot.sender_name)
    else:
        contact_root_message(tbot, message)
        tbot.block_user_attempts.append(tbot.sender_name)


def contact_root_message(tbot, message):
    """
    Send a specific message to the root users

    :param tbot: The telegrambot itself
    :param message: The string of text that the bot should send to the user
    :return: Returns the status code to the user
    """
    try:
        root_users = tbot.data["userlevels"]["root"]
        for user_id in root_users:
            params = {'chat_id': int(user_id), 'text': message}
            method = 'sendMessage'
            requests.post(tbot.api_url + method, params)
        logging.debug("Send '" + message + "' to the root")
        return "200"
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"
