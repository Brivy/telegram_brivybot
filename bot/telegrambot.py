"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


telegrambot.py
---------------
The file/class with the logic of the telegram bot.
This class initializes the bot and has all the functions
to handle incoming and outgoing messages
"""
import logging
import re
import threading
import requests
import json
from bot import tbot_send, tbot_message, refresh_json
from commands.hardware_commands import multi_thread_time


class Tbot:

    def __init__(self, token):
        """
        Initialize the bot
        The bot needs the following parameters to work:
            - A lock: This bot will sometimes be locked when an user requests a sub-menu
            - first_run: True on startup of the bot (first run initializes some parameters)
            - data: This bot gets his user data from the data.json file
            - A token: This is the identifier of the bot
            - An api_url: This is the first part of the telegram bot API and it needs a token to work
            - A new_offset: The bot will filter all messages with this number (this number
              can be reached with the method "get_last_update")
            - A sender_id: The bot needs the ID of the user, so that he can send messages to the right user
            - A sender_name: The bot needs the name of the user for sending specific messages
            - block_user_attempts: This array contains users that spam to the bot

        At last, the bot starts threads
        The first thread is a automatic data update thread:
            This is because the data file doesn't need to be read all the time
            Using this thread, the data will be updated every 30 seconds
        The second thread is a timed thread:
            This is because the root user wants to receive a weather message on specific times
            These times can be configured in the thread itself
        :param token: Used to access the bot (this is different for each bot)
        """
        with open('data/database/data.json') as f:
            data = json.load(f)

        self.log_counter = 0
        self.lock = False
        self.first_run = True
        self.data = data
        self.token = token
        self.api_url = data["telegram"]["api"].format(token)
        self.new_offset = self.get_last_update()
        self.sender_id = None
        self.sender_name = None
        self.block_user_attempts = []
        refresh_json.JsonRefresher(self, "data_thread", 1).start()
        multi_thread_time.MultiThreadTime(self, "time_thread", 2).start()


    def get_updates(self, new_install, thread_sender, offset=None, timeout=1800):
        """
        Get the last message updates from the telegram API

        Important! The sender of a message is going to be verified!
        If the sender is not in the data sheet, then this user will be added to the block list

        If the response from telegram is a 409, then multiple instances are running
        These errors only occur when the main thread will have a constant search for new messages
        and an user starts an sub-menu
        When this happens, stop the main thread and only process the sub-menu messages

        Because some edited messages can crash the program, these will be caught in the check_edited_messages function
        When one has been found, change the offset to scan for the next message and recall this function until
        a message without a edit has been found

        :param new_install: If true, get_updates has to take in account that this is a new install of the telegrambot
        :param thread_sender: None if no thread users are presented otherwise this must contain the sender_id
        from the thread user
        :param offset: check if a update has already been seen (default is None because there
        not always updates available (especially the very first message))
        :param timeout: After no new updates are encountered, enable a delay (default is 30 seconds)
        :return: Return the update in json
        """
        try:
            method = 'getUpdates'
            params = {'timeout': timeout, 'offset': offset}
            resp = requests.get(self.api_url + method, params)

            # Got two instance error? Return
            if "409" in str(resp):
                if threading.current_thread() == threading.main_thread():
                    logging.warning("409 error found within main thread, locking main thread..")
                    self.lock = True
                    return str(resp)
                logging.warning("Processing submenu for " + str(threading.current_thread()))
                return

            # Convert the result to JSON
            result_json = resp.json()['result']

            # Empty? Restart
            if not result_json:
                self.log_counter += 1
                if self.log_counter == 30:
                    logging.info("Telegram update: it is still alive")
                    self.log_counter = 0
                return

            # Check the json for errors
            if not self.get_updates_check(result_json, thread_sender, new_install):
                self.new_offset += 1
                logging.warning("No valid message found")
                return

            return result_json
        except requests.exceptions.ConnectionError:
            tbot_send.send_message(self, "404: No connection could be made")
            logging.error("The response object cannot connect to the internet")
            return


    def get_last_update(self):
        """
        Get the send message in the getUpdate telegram API

        If there are no new updates in the telegram API (an empty string[]), set the offset to None
        When new updates are found, get only the last new update (discard the others)

        :return: returns the last update ID OR None is no new updates are found
        """
        get_result = self.get_updates(False, None)
        if not get_result:
            return None
        last_update = get_result[-1]["update_id"]
        logging.info("Initializing new offset")
        return last_update


    def set_user_id(self, result_json):
        """
        Set the sender_id and the sender_name based on the id in the result_json

        :param result_json: The result of the message converted to JSON
        :return: No returns
        """
        if re.search(r'edited_message', str(result_json)) is None:
            self.sender_id = result_json[-1]["message"]["from"]["id"]
            self.sender_name = result_json[-1]["message"]["from"]["first_name"]
        else:
            self.sender_id = result_json[-1]["edited_message"]["from"]["id"]
            self.sender_name = result_json[-1]["edited_message"]["from"]["first_name"]
        logging.debug("Got sender ID")


    def get_updates_check(self, result_json, thread_sender, new_install):
        """
        This function will check the result on the three most common problems:
            - Check if a lock has been set and if so, check if the correct user
            send a message
            - Set the sender id and name (no check, but required)
            - Check the access file (if a blocked person sends a message, don't let it through)
            - Check if the message is an edit

        When one of these problems are found, return False
        Otherwise return True

        :param result_json: The result of the message converted to JSON
        :param thread_sender: None if no thread users are presented otherwise this must contain the sender_id
        from the thread user
        :param new_install: If true, get_updates has to take in account that this is a new install of the telegrambot
        :return: True if the message is correct and False otherwise
        """

        # If lock is true, check if the correct sender from a thread sends a message
        if self.lock is True and thread_sender != result_json[-1]["message"]["from"]["id"]:
            logging.warning("Main thread is locked and the sender is not the correct sender")
            return False

        # Get the user_id from the right json string
        self.set_user_id(result_json)

        # Check the access file
        if not self.check_access_file(new_install):
            return False

        # Check for edited message
        if hasattr(self, "new_offset") and tbot_message.check_edited_message(result_json):
            logging.warning("Messages cannot be edited")
            tbot_send.send_message(self, "No edits allowed")
            return False

        return True


    def check_access_file(self, new_install):
        """
        Check the access file in case the user is new or blocked
        In both cases, send a message to the root user

        If this is a first run (when the bot starts up, this is true) and a new
        install (occurs when a new install takes place), skip the data file
        check (because it doesn't have any users)

        :param new_install: If true, get_updates has to take in account that this is a new install of the telegrambot
        :return: True if the user has access or False otherwise
        """
        if not self.first_run and not new_install:
            # Check for illegal access
            user_level = self.check_user_file()
            if hasattr(self, "new_offset") and re.match(r'^(block|new)$', user_level) is not None:
                if user_level == "block":
                    tbot_send.send_intruder_message(self)
                else:
                    tbot_send.send_new_user_message(self)
                return False
            else:
                return True
        else:
            self.first_run = False
            logging.info("First run complete")
            return True


    def check_user_file(self):
        """
        There is a JSON file stored within this project that holds all (verified) user data
        The user will be verified with the following levels:
            - The root check: Is the user root, return root
            - The user check: Is the user a normal user, return user
            - The block check: Is the user blocked, return block
            - The user has not been found: Create a new entry in the data.json file and notify the root

        :return: True if the user is allowed to send messages, False if the user is not allowed
        """
        users = self.data["userlevels"]["user"]
        root_users = self.data["userlevels"]["root"]
        blocked = self.data["userlevels"]["block"]

        # root check
        for user_id in root_users:
            if str(self.sender_id) == str(user_id):
                logging.info("User has root user access")
                return "root"

        # user check
        for user_id in users:
            if str(self.sender_id) == str(user_id):
                logging.info("User has normal user access")
                return "user"

        # block check
        for user_id in blocked:
            if str(self.sender_id) == str(user_id):
                logging.warning("User " + str(self.sender_id) + " is blocked, no access allowed")
                return "block"

        # user has not been found
        if hasattr(self, "new_offset"):
            new_data = {self.sender_id: self.sender_name}
            self.data["userlevels"]["block"].update(new_data)

            with open('data/database/data.json', 'w') as f:
                json.dump(self.data, f)

            logging.warning("User " + str(self.sender_id) + " is new, no access allowed")
            return "new"

        return None
