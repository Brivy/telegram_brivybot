"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


refresh_json.py
---------------
This Threading class has been build to update
the json data every 30 seconds
"""

import json
import logging
import threading
import time

class JsonRefresher (threading.Thread):


    def __init__(self, tbot, name, counter):
        """
        Initialize the json update thread
        This thread needs the following parameters ot work:
            - The tbot: The telegrambot that he needs to update
            - A thread_id: This way, you can recognize the thread in the main program
            - A name: The thread must be named
            - A counter: Counts how many threads there have been

        :param tbot: The telegrambot itself
        :param name: The name of the thread
        :param counter: The ID and the counter of the thread
        """
        threading.Thread.__init__(self)
        self.tbot = tbot
        self.thread_id = counter
        self.name = name
        self.counter = counter
        logging.debug("Initialized jsonRefresher thread")


    def run(self):
        """
        While this thread is running, the data.json file can be updated
        The new file will replace the old file every 30 seconds, even if
        the file is the same as the old one
        This is because calculating a checksum to check if they are still the same
        is way slower than just replacing the file over and over

        :return: This function does not stop
        """
        logging.info("Starting thread JsonRefresher")
        while True:
            with open('data/database/data.json') as f:
                data = json.load(f)
            self.tbot.data = data
            time.sleep(10)
