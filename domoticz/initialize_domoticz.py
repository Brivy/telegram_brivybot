"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


initialize_domoticz.py
---------------
This file initializes domoticz and all
of his components
Most of the API's are found within the
"data/database/data.json" file
"""

import json
import logging

import requests


class Domoticz:

    def __init__(self):
        """
        Initialize domoticz
        Domoticz has the following parameters:
            - dom_links: this is a shortcut to the domoticz file and
              grabs all the links within the data.json file
            - username: the username to access the API from domoticz
            - password: the password from domoticz
            - home_url: the home url combined with the username and password
        """
        with open('data/database/data.json') as f:
            data = json.load(f)

        self.dom_links = data["domoticz"]["links"]
        self.username = data["domoticz"]["userinfo"]["username"]
        self.password = data["domoticz"]["userinfo"]["password"]
        self.home_url = str(self.dom_links["base"]["home"] +
                            self.dom_links["base"]["security"].format(self.username, self.password))

    def test_auth_domoticz_api(self):
        """
        Test if the bot has the correct authorization
        for domoticz

        :return: True if it has the correct data OR False when this is not correct
        """
        resp = requests.get(str(self.home_url + self.dom_links["command"]["getInfo"]),
                            verify=False, timeout=20)
        if "SystemName" not in str(resp.json()):
            logging.warning("No connection to Domoticz established")
            return False
        logging.info("Connection to Domoticz established")
        return True
