"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


domoticzSetup.py
---------------
This file initializes domoticz and all
of his components
Most of the API's are found within the
"data/database/data.json" file
"""

import json
import logging
import re
import requests
from bot import tbot_send, tbot_message


class DomoticzNewInstall:

    def __init__(self, tbot):
        """
        Initialize a new install of domoticz!
        This object will only be created when a new install of the telegrambot is requested

        This version of Domoticz has the following parameters:
            - dom_links: This is a shortcut to the domoticz file and
            graps all the links within the data.json file
            - IP: The ip address of the domoticz server
            This is None at the start of the installer
            - port: The port of the domoticz server
            - username: The username to access the API from domoticz
            - password: The password from domoticz
            - home = A modified IP link with http and parameters
            - authenticate = A string containing the username and the password
            - home_authenticate: The home url combined with the authenticate string

        A lot of the variables are shortcuts, but with the right implementation
        they save a lot of space and confusion within the code
        """
        self.dom_links = tbot.data["domoticz"]["links"]
        self.home = self.dom_links["base"]["home"]

        if self.dom_links["base"]["ip"] and self.dom_links["base"]["port"]:
            self.ip = tbot.data["domoticz"]["links"]["base"]["ip"]
            self.port = tbot.data["domoticz"]["links"]["base"]["port"]
        elif self.home:
            home_parts = re.match(r'^http://(\d+\.\d+\.\d+\.\d+|localhost):(\d+)/json.htm\?$', self.home)
            self.ip = home_parts.group(1)
            self.port = home_parts.group(2)

            ip_json = {"ip": self.ip}
            port_json = {"port": self.port}

            tbot.data["domoticz"]["links"]["base"].update(ip_json)
            tbot.data["domoticz"]["links"]["base"].update(port_json)

            with open('data/database/data.json', 'w') as f:
                json.dump(tbot.data, f)
        else:
            self.ip = None
            self.port = None

        self.username = tbot.data["domoticz"]["userinfo"]["username"]
        self.password = tbot.data["domoticz"]["userinfo"]["password"]
        self.authenticate = self.dom_links["base"]["security"].format(self.username, self.password)
        self.home_authenticate = str(self.home + self.authenticate)


    def initialize_ip_port_domoticz(self, tbot):
        """
        Initialize the ip address and port of the domoticz server
        This will happen in two loops:
            - The first loop will get the ip address
            - The second loop will get the port

        After this configuration, the home_url will be tested
        The result of this test can see if an username and a password are required

        :param tbot: The telegrambot itself
        :returns: No returns
        """
        while True:
            tbot_send.send_message(tbot, "Now we are initialize your domoticz server\n"
                                         "Please enter your domoticz IP address\n"
                                         "If the domoticz server is running on the same device, enter "
                                         "(without quotes) 'localhost'\n"
                                         "Otherwise, enter something like '123.123.123.123'")
            # Loop 1: Check valid IP
            while True:
                domoticz_ip = tbot_message.get_message(tbot, None, True)
                if re.match(r'^((\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})|(localhost))$', domoticz_ip) is None:
                    logging.warning("IP must comply with the following regex: ^(\d{1,3})\.(\d{1,3})\."
                                    "(\d{1,3})\.(\d{1,3})|(localhost)$")
                    tbot_send.send_message(tbot, "Please enter a valid IP address")
                else:
                    self.ip = domoticz_ip
                    domoticz_ip_json = {"ip": domoticz_ip}
                    tbot.data["domoticz"]["links"]["base"].update(domoticz_ip_json)
                    logging.info("Entered a correct IP")
                    break

            tbot_send.send_message(tbot, "Great!\n"
                                         "Now I require the port of the server\n"
                                         "In most cases, this is 8080 but it is possible that you "
                                         "configured it on a different port\n"
                                         "The port can be found right next to your ip address "
                                         "like this: '" + self.ip + ":8080'")

            # Loop 2: Check valid port (1-65535)
            while True:
                domoticz_port = tbot_message.get_message(tbot, None, True)
                if re.match(r'^([0-9]|[1-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9][0-9][0-9]|[1-6][0-9][0-9][0-9][0-5])$',
                            domoticz_port) is None:
                    logging.warning("Port must comply with the following regex: ^([0-9]|[1-9][0-9]|[1-9][0-9][0-9]|"
                                    "[1-9][0-9][0-9][0-9]|[1-6][0-9][0-9][0-9][0-5])$")
                    tbot_send.send_message(tbot, "Please enter a valid port number")
                else:
                    self.port = domoticz_port
                    domoticz_port_json = {"port": domoticz_port}
                    self.dom_links["base"].update(domoticz_port_json)
                    logging.info("Entered a correct port")
                    break

            # Set the home url with the IP and the port
            self.home = str("http://" + domoticz_ip + ":" + domoticz_port + "/json.htm?")
            home_url = {"home": self.home}
            tbot.data["domoticz"]["links"]["base"].update(home_url)

            # Write the home url to the database
            with open('data/database/data.json', 'w') as f:
                json.dump(tbot.data, f)

            logging.info("Wrote IP, port and home to the data.json file")

            # Get and return the result of the test
            return self.test_domoticz_url(tbot)


    def test_domoticz_url(self, tbot):
        """
        Test The current IP and port of the domoticz settings
            - If status is not found, then no connection has been found
            - If the status is found but not the SystemName, that means that
              the correct IP and port has been used but that the site is
              protected with a username and password
            - If the status and the SystemName are found, than the correct
             settings are used and no username and password are used

        :param tbot: The telegrambot itself
        :return: True if no errors are found OR False when errors are encountered
        """
        try:
            resp = requests.get(str(self.home + self.authenticate + self.dom_links["command"]["getInfo"]),
                                verify=False, timeout=20)
            if "status" not in str(resp.json()):
                tbot_send.send_message(tbot, "No connection to domoticz could be established\n"
                                             "This means that you have one of the following incorrect settings:\n\n"
                                             " - A wrong IP or localhost\n"
                                             " - The port is not open\n"
                                             "Check if you can reach http://" + self.ip + ":" + self.port + "\n\n")
                logging.warning("Wrong IP or port used")
                return "404"

            elif "SystemName" not in str(resp.json()):
                tbot_send.send_message(tbot, "Connection with domoticz established but requires an username "
                                             "and password")
                logging.info("Connection requires an username and a password")
                return "401"

            tbot_send.send_message(tbot, "Connection with domoticz established!")
            logging.info("Connection successful")
            return "200"
        except requests.exceptions.ConnectTimeout:
            tbot_send.send_message(tbot, "Timeout on request\n"
                                         "You probably got the wrong IP address/port")
            logging.warning("Timeout, probably used a wrong IP or port")
            return False
        except requests.exceptions.ConnectionError:
            tbot_send.send_message(tbot, "Connection Error\n"
                                         "You probably got the wrong IP address/port")
            logging.warning("Connection error, probably used a wrong IP or port")
            return False


    def test_domoticz_login(self, tbot):
        """
        Test the username and password of domoticz
        This function will try to post a log message to domoticz and with the right username and
        password this returns a 200 (otherwise 401)

        :param tbot: The telegrambot itself
        :return: True if no errors are found OR False when errors are encountered
        """
        try:
            resp = requests.get(self.home_authenticate + self.dom_links["command"]["logTo"]
                                .format("Successful root login"), verify=False, timeout=20)

            if "200" not in str(resp):
                tbot_send.send_message(tbot, "No connection to domoticz could be established\n"
                                             "This means that you have one of the following incorrect settings:\n\n"
                                             " - A wrong IP or localhost\n"
                                             " - The port is not open\n"
                                             "Check if you can reach http://" + self.ip + ":" + self.port + "\n\n"
                                             "- A wrong username/password\n"
                                             "If the first link brings you to the login screen of domoticz, "
                                             "than your username/password is incorrect\n"
                                             "Check with the following link of everything works:\n\n" +
                                             str(self.home_authenticate) + "\n")
                logging.warning("No successful connection established, probably due to a wrong username/password")
                return False
            tbot_send.send_message(tbot, "Successful login to the domoticz server!")
            logging.info("Successful login")
            return True
        except requests.exceptions.ConnectTimeout:
            tbot_send.send_message(tbot, "Timeout on request\n"
                                         "You probably got the wrong IP address/port")
            logging.warning("Timeout, probably used a wrong IP or port")
            return False
        except requests.exceptions.ConnectionError:
            tbot_send.send_message(tbot, "Connection Error\n"
                                         "You probably got the wrong IP address/port")
            logging.warning("Connection error, probably used a wrong IP or port")
            return False
