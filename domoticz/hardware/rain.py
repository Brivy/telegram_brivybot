"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


rain.py
--------
This is a hardware file that initializes a rain meter
"""

import re

from bot import tbot_send
from domoticz.hardware import device


class Rain(device.Device):

    def __init__(self, idx):
        """
        Initialize a rain meter.
        For example, this can be a real rain meter of a Darksky API call

         The following attributes are from the superclass "device":
             - A domoticz object - This is mainly used to get the right API's for the URL
             - An idx - This is the ID of the switch and is mainly used in API calls
             - An url - With this url, every attribute of the switch can be retrieved
             - A json_url = A json format of the domoticz api that contains all the information
             over the device

         This meter has the following attributes:
             - There are no special attributes for this device

         :param idx: Id of the rain meter
         """
        super(Rain, self).__init__(idx)
        self.regex_pattern = re.compile(r'^(\d{1,2});(\d\.\d)$')


    def get_data(self):
        """
        Get the data of the meter

        :return: Return the parts of the data result
        """
        resp_json = self.json_url["result"][0]["Data"]
        resp_parts = re.match(self.regex_pattern, resp_json)
        if resp_parts is None:
            return "404: No rain meter data found"
        return resp_parts


    def return_compact_data(self, tbot, keyboard):
        """
        This function will generate a message with the rate of the rain

        :param tbot: The telegrambot itself
        :param keyboard: The keyboard style in JSON
        :return: Return the status code
        """

        # Get all variables
        response = self.get_data()
        if "404" in str(response):
            return response
        rain = response.group(1)
        rain_rate = response.group(2)

        tbot_send.send_message(tbot, "**************************************\n"
                               "This is the local weather (mijdrecht):\n"
                               "Number of rain drops: " + rain + "\n"
                               "Rate of the rain: " + rain_rate + "\n"
                               "**************************************", keyboard)
        return "200"
