"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


custom_device.py
--------
This is a hardware file that initializes a custom sensor
"""
from bot import tbot_send
from domoticz.hardware import device


class CustomDevice(device.Device):

    def __init__(self, idx):
        """
        Initialize a Custom meter.
        For example, this can be a ozone meter (in domoticz configured as a custom device)

         The following attributes are from the superclass "device":
             - A domoticz object - This is mainly used to get the right API's for the URL
             - An idx - This is the ID of the switch and is mainly used in API calls
             - An url - With this url, every attribute of the switch can be retrieved
             - A json_url = A json format of the domoticz api that contains all the information
             over the device

         This meter has the following attributes:
             - There are no special attributes for this device

         :param idx: Id of the visibility meter
         """
        super(CustomDevice, self).__init__(idx)


    def return_compact_data(self, tbot, keyboard):
        """
        Return data of the device

        :param tbot: The telegrambot itself
        :param keyboard: A JSON compacted keyboard
        :return: A status code
        """
        data = self.get_data()
        if "404" in str(data):
            return data

        tbot_send.send_message(tbot, "**************************************\n"
                                     "This is the data from a custom sensor:\n"
                                     "Data: " + data + "\n"
                                     "**************************************", keyboard)
        return "200"
