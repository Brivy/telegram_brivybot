"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


device.py
----------------
Superclass of all the devices
"""

import requests

from bot import tbot_send
from domoticz import initialize_domoticz


class Device:
    def __init__(self, idx):
        """
        Superclass of all the devices in domoticz
        Some devices have the same functions and attributes

        This superclass has the following attributes:
            - A domoticz object - This is mainly used to get the right API's for the URL
            - An idx - This is the ID of the switch and is mainly used in API calls
            - An url - With this url, every attribute of the switch can be retrieved
            - A json_url = A json format of the domoticz api that contains all the information
            over the device

        :param idx: Id of the switch
        """
        self.domoticz = initialize_domoticz.Domoticz()
        self.idx = idx
        self.url = self.domoticz.home_url + self.domoticz.dom_links["device"]["getDevice"].format(idx)
        self.json_url = self.set_json_url().json()


    def get_data(self):
        """
        Get the data from the device

        :return: Return the data of the device
        """
        return self.json_url["result"][0]["Data"]


    def get_device_name(self):
        """
        Get the name of the device

        :return: The name of the device
        """
        return self.json_url["result"][0]["Name"]


    def get_last_update(self):
        """
        Get the last update from the device

        :return: The last update
        """
        return self.json_url["result"][0]["LastUpdate"]


    def set_json_url(self):
        """
        Set the json_url to the temp_hum object

        :return: The json file OR an error is the API is not reachable
        """
        try:
            return requests.get(self.url)
        except requests.exceptions.ConnectionError:
            return "404: Connection timed out"


    def return_compact_data(self, tbot, keyboard):
        """
        Return data of the device

        :param tbot: The telegrambot itself
        :param keyboard: A JSON compacted keyboard
        :return: A status code
        """
        tbot_send.send_message(tbot, "**************************************\n"
                               "This is the local data (extracted from result):\n"
                               + self.json_url +
                               "**************************************", keyboard)
        return "200"
