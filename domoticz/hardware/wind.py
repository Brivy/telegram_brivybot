"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


wind.py
--------
This is a hardware file that initializes a wind meter
"""

import re

from bot import tbot_send
from domoticz.hardware import device


class Wind(device.Device):

    def __init__(self, idx):
        """
        Initialize a wind meter.
        For example, this can be a real wind meter of a Darksky API call

         The following attributes are from the superclass "device":
             - A domoticz object - This is mainly used to get the right API's for the URL
             - An idx - This is the ID of the switch and is mainly used in API calls
             - An url - With this url, every attribute of the switch can be retrieved
             - A json_url = A json format of the domoticz api that contains all the information
             over the device

         This meter has the following attributes:
             - There are no special attributes for this device

         :param idx: Id of the wind meter
         """
        super(Wind, self).__init__(idx)
        self.regex_pattern = re.compile(r'^(\d+\.\d+);(\w+);(\d)(\d{1,3});(\d{1,3})(\d);(\d+\.\d);(\d+\.\d)$')


    def get_data(self):
        """
        Get the data of the device

        :return: Return the parts of the data result
        """
        resp_json = self.json_url["result"][0]["Data"]
        resp_parts = re.match(self.regex_pattern, resp_json)
        if resp_parts is None:
            return "404: No wind meter data found"
        return resp_parts


    def return_compact_data(self, tbot, keyboard):
        """
        This function will generate a message with the speed of the wind

        :param tbot: The telegrambot itself
        :param keyboard: The keyboard style in JSON
        :return: Return the status code
        """

        # Get all variables
        response = self.get_data()
        if "404" in str(response):
            return response

        degrees = response.group(1)
        direction = response.group(2)
        speed = response.group(3) + "." + response.group(4)
        gust = response.group(5) + "." + response.group(6)
        temp = response.group(7)

        tbot_send.send_message(tbot, "**************************************\n"
                                     "This is the local weather:\n"
                                     "Temperature: " + temp + " C\n"
                                     "Windspeed: " + speed + " m/s\n"
                                     "Gustspeed: " + gust + "m/s\n"
                                     "Wind direction: " + direction + " (" + degrees + u"\u00b0)\n"
                                     "**************************************", keyboard)
        return "200"
