"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


temp_hum.py
--------
This is a hardware file that initializes a temperature meter and
the humidity of the air
At the moment when initializing this meter, the bot expects
that the temperature meter is reachable with the "domoticz" API
"""

import re

from bot import tbot_send
from domoticz.hardware import device


class TempHum(device.Device):

    def __init__(self, idx):
        """
        Initialize a temperature and humidity meter.
        For example, this can be a home made one or for example the Darksky API

        The following attributes are from the superclass "device":
            - A domoticz object - This is mainly used to get the right API's for the URL
            - An idx - This is the ID of the switch and is mainly used in API calls
            - An url - With this url, every attribute of the switch can be retrieved
            - A json_url = A json format of the domoticz api that contains all the information
            over the device

        This meter has the following attributes:
            - There are no special attributes for this device

        :param idx: Id of the switch
        """
        super(TempHum, self).__init__(idx)
        self.regex_pattern = re.compile(r'^(\d+.\d C), (\d+ %), (\d+ hPa)$')


    def get_data(self):
        """
        Get the data of the device

        :return: Return the parts of the data result
        """
        resp_json = self.json_url["result"][0]["Data"]
        resp_parts = re.match(self.regex_pattern, resp_json)
        if resp_parts is None:
            return "404: No (temp/hum/pressure) match found"
        return resp_parts


    def get_forecast(self):
        """
        Get the weather forecast in string

        :return: The weather forecast
        """
        return self.json_url["result"][0]["ForecastStr"]


    def get_humidity_status(self):
        """
        Get the humidity status in string

        :return: The humidity status
        """
        return self.json_url["result"][0]["HumidityStatus"]


    def return_compact_data(self, tbot, keyboard):
        """
        This function will generate a weather info message
        It will not work with all the weather devices!

        :param tbot: The telegrambot itself
        :param keyboard: The keyboard style in JSON
        :return: Return the status code
        """

        # Get all variables
        response = self.get_data()
        if "404" in str(response):
            return response

        temp = response.group(1)
        hum = response.group(2)
        bar = response.group(3)
        last_update = self.get_last_update()
        forecast = self.get_forecast()
        humidity_string = self.get_humidity_status()

        tbot_send.send_message(tbot, "**************************************\n"
                               "This is the local weather (mijdrecht):\n"
                               "Temperature: " + temp + "\n"
                               "Humidity: " + hum + "\n"
                               "Air pressure: " + bar + "\n"
                               "**************************************\n"
                               "This is the forecast of today: " + forecast + "\n"
                               "This is the status of the humidity: " + humidity_string + "\n"
                               "Last update: " + last_update + "\n"
                               "**************************************", keyboard)
        return "200"
