"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


uv_index.py
--------
This is a hardware file that initializes a visibility meter
"""

import re

from bot import tbot_send
from domoticz.hardware import device


class Visibility(device.Device):

    def __init__(self, idx):
        """
        Initialize a visibility meter.
        For example, this can be a real visibility meter of a Darksky API call

         The following attributes are from the superclass "device":
             - A domoticz object - This is mainly used to get the right API's for the URL
             - An idx - This is the ID of the switch and is mainly used in API calls
             - An url - With this url, every attribute of the switch can be retrieved
             - A json_url = A json format of the domoticz api that contains all the information
             over the device

         This meter has the following attributes:
             - There are no special attributes for this device

         :param idx: Id of the visibility meter
         """
        super(Visibility, self).__init__(idx)
        self.regex_pattern = re.compile(r'^(\d{1,2}\.\d km)$')


    def get_data(self):
        """
        Get the data of the meter

        :return: Return the parts of the data result
        """
        resp_json = self.json_url["result"][0]["Data"]
        resp_parts = re.match(self.regex_pattern, resp_json)
        if resp_parts is None:
            return "404: No visibility meter data found"
        return resp_parts


    def return_compact_data(self, tbot, keyboard):
        """
        This function will generate a message with the current visibility

        :param tbot: The telegrambot itself
        :param keyboard: The keyboard style in JSON
        :return: Return the status code
        """

        # Get all variables
        response = self.get_data()
        if "404" in str(response):
            return response
        visibility = response.group(1)

        tbot_send.send_message(tbot, "**************************************\n"
                               "This is the local weather (mijdrecht):\n"
                               "Visibility: " + visibility + "\n"
                               "**************************************", keyboard)
        return "200"
