"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


LightSwitch.py
--------
This is a hardware file that initializes a switch
At the moment when initializing a switch,
the bot expects that the switch is reachable with the "domoticz" API
"""
import requests
from bot import tbot_send
from domoticz.hardware import device


class LightSwitch(device.Device):

    def __init__(self, idx):
        """
        Initialize a switch.
        For example, this can be a light bulb or a "klik aan, klik uit"

         The following attributes are from the superclass "device":
             - A domoticz object - This is mainly used to get the right API's for the URL
             - An idx - This is the ID of the switch and is mainly used in API calls
             - An url - With this url, every attribute of the switch can be retrieved
             - A json_url = A json format of the domoticz api that contains all the information
             over the device

         This meter has the following attributes:
             - There are no special attributes for this device

         :param idx: Id of the switch
         """
        super(LightSwitch, self).__init__(idx)


    def change_status(self, tbot, keyboard=None):
        """
        Change the status ("data") of the switch
        Only "On" and "Off" are allowed

        :return: Returns the status code to the user
        """
        status = self.json_url["result"][0]["Data"]
        if status == "On":
            status = "Off"
        else:
            status = "On"
        if keyboard:
            tbot_send.send_message(tbot, "Switching light to '" + status + "'", keyboard)
        else:
            tbot_send.send_message(tbot, "Switching light to '" + status + "'")
        url = str(self.domoticz.home_url + self.domoticz.dom_links["lights"]["switchLight"].format(self.idx, status))
        resp = requests.get(url)
        return resp


    def is_light_switch(self):
        """
        Check if the switch is a "Light/Switch"
        Otherwise, return an error to the user

        :return: The status code
        """
        if "result" in str(self.json_url):
            if self.json_url["result"][0]["Type"] != 'Light/Switch':
                return "404: No light switch found"
            self.status = self.get_data()
            return "200"
        else:
            return "404: Incorrect idx"
