"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


menu.py
-------
This file handles all the menu's within the bot
The most used menu "bot_menu" is always running and
handles all of the users requests
"""
import logging
import threading
import time
from bot import tbot_send, tbot_message
from bot.keyboard import initialize_keyboard
from commands import commands


def startup_menu(tbot):
    """
    Startup menu
    Users have to press /start to continue to the second menu

    :param tbot: The telegrambot itself
    :return: This function does not return anything
    """
    keyboard = initialize_keyboard.start_menu_keyboard()
    while True:
        message = tbot_message.get_message(tbot)
        if message is not None and message == "/start":
            thread_count_id = threading.active_count() + 1
            thread_name = "Thread: " + str(tbot.sender_id) + " - " + tbot.sender_name
            commands.CommandThreading(tbot, thread_name, thread_count_id, message).start()
            logging.info("Bot started")
            break
        tbot_send.send_message(tbot, "Press /start to continue", keyboard)


def bot_menu(tbot):
    """
    This menu handles all the incoming messages (not including the sub-menu messages)
    When it receives a message, it will check if it is a 409 error (multiple running instances
    of the telegrambot)

    - When no multiple instances are found, start a new thread with the message as new command for the bot
    - When multiple instances are found, lock every message behind a while loop
      If all the commanding threads are finished, break the while loop

    :param tbot: The telegrambot itself
    :return: This function does not return anything
    """
    try:
        while True:
            message = tbot_message.get_message(tbot)
            logging.info("Command: " + message)
            if "409" in message:
                tbot_send.send_message_to_all_users(tbot, "Edits in progress..\n"
                                                          "All commando's are on a cooldown until are done")

                while True:
                    threads_list = threading.enumerate()
                    if "CommandThreading" not in str(threads_list):
                        break
                    time.sleep(1)
                logging.info("Lock revoked")
            else:
                thread_count_id = threading.active_count() + 1
                thread_name = "Thread: " + str(tbot.sender_id) + " - " + tbot.sender_name
                commands.CommandThreading(tbot, thread_name, thread_count_id, message).start()
    except KeyboardInterrupt:
        logging.error("Application stopped by keyboard")
    except Exception as e:
        logging.error(e)
        tbot_send.send_message(tbot, "Error 900: Something went terribly wrong before starting the threads, see the logs for more info)")
