"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


new_install.py
-------
This file will be called every time the bot starts up
It will check for a data.json file and when no root user
within this file has been found, create a new one
"""
import json
import logging
import re
import time

from bot import tbot_send, tbot_message
from domoticz import domoticz_new_install
import base64

def check_for_new_install(tbot):
    """
    Check if this is a new bot
    This function will check if there is a root user presented
    If not, ask for data about the domoticz server

    :param tbot: The telegrambot itself
    :return: True when the function ends
    """
    if tbot.data["userlevels"]["root"]:
        logging.info("Root user found, starting bot")
        return True

    logging.warning("No root user found, initializing bot")
    tbot_message.get_message(tbot, None, True)
    tbot_send.send_message(tbot, "Welcome to this domoticz bot\n"
                                 "We detected no root users within the data.json file, so we will create one for you!\n"
                                 "(This process is automatic, so don't worry)")

    while True:
        if configure_new_install(tbot):
            break
    logging.info("Bot initialized, continue..")
    return True


def configure_new_install(tbot, clean=True):
    """
    Configure all the settings for the data.json file
    This function will create the following objects for the data.json file:
        - An IP address
        - A port for the IP address
        - An username and password when needed
        - A root user

    :param tbot: The telegrambot itself
    :param clean: True to clean the above list and False to not clean this
    list (default = True)
    :return: True when no errors are found OR False when a wrong internet address has been used
    """

    # Clear the most important point within the config (when looping)
    if clean:
        logging.info("Clean data.json file")
        clean_install(tbot)

    # Test for IP and port
    domoticz = domoticz_new_install.DomoticzNewInstall(tbot)
    connection_status = domoticz.initialize_ip_port_domoticz(tbot)

    if not connection_status or connection_status == "404":
        return False
    elif connection_status == "401":
        check_user_login(tbot)

    # Create a new root user and add him to the data.json file
    create_new_root_user(tbot)
    return True


def clean_install(tbot):
    """
    Remove the essential parts out of the data.json file

    :param tbot: The telegrambot itself
    :return: No returns
    """
    tbot.data["domoticz"]["links"]["base"].update({"ip": ""})
    tbot.data["domoticz"]["links"]["base"].update({"port": ""})
    tbot.data["domoticz"]["links"]["base"].update({"home": ""})
    tbot.data["domoticz"]["userinfo"].update({"username": ""})
    tbot.data["domoticz"]["userinfo"].update({"password": ""})

    with open('data/database/data.json', 'w') as f:
        json.dump(tbot.data, f)


def authenticate_user(tbot):
    """
    Initialize the username and password
    These variables are going to be base64 encoded (for
    the API request to domoticz)

    :param tbot: The telegrambot itself
    :return: No returns
    """
    tbot_send.send_message(tbot, "Okido\n"
                                 "Firstly, enter your username:")
    username = tbot_message.get_message(tbot, None, True)
    tbot_send.send_message(tbot, "Secondly, enter your password:")
    password = tbot_message.get_message(tbot, None, True)

    # Base64 encode the username and password
    encode_username = base64.b64encode(bytes(username, 'ascii'))
    encode_password = base64.b64encode(bytes(password, 'ascii'))
    domoticz_username = {"username": str(encode_username.decode("utf-8"))}
    domoticz_password = {"password": str(encode_password.decode("utf-8"))}
    tbot.data["domoticz"]["userinfo"].update(domoticz_username)
    tbot.data["domoticz"]["userinfo"].update(domoticz_password)
    logging.info("Writing userdata to the data.json file")

    with open('data/database/data.json', 'w') as f:
        json.dump(tbot.data, f)


def check_user_login(tbot):
    """
    Check if the user wants to enter a username and password and handle the request
    The while loop makes sure that the user can enter a wrong username and password
    a few times
    It is also possible to leave this function with an incorrect username/password

    :param tbot: The telegrambot itself
    :return: No returns
    """
    tbot_send.send_message(tbot, "Enter 'Yes' if you have an username and password and 'No' if you "
                                 "want to configure it later")
    authenticate_check = tbot_message.get_message(tbot, None, True)
    if re.match(r'^[Yy]es$', authenticate_check) is not None:
        logging.info("Authentication start")
        while True:
            authenticate_user(tbot)

            # Test login to domoticz
            domoticz = domoticz_new_install.DomoticzNewInstall(tbot)
            if domoticz.test_domoticz_login(tbot):
                return True
            else:
                tbot_send.send_message(tbot, "Do you want to re-enter your username and password?\n"
                                             "Please enter 'Yes' or 'No'")
                confirmation = tbot_message.get_message(tbot, None, True)
                if re.match(r'^[Yy]es$', confirmation) is None:
                    logging.info("Continuing without a valid username/password")
                    break
                logging.info("Retry username/password")


def create_new_root_user(tbot):
    """
    Create a new root user with the sender ID

    :param tbot: The telegrambot itself
    :return: No returns
    """
    new_root_user = {tbot.sender_id: tbot.sender_name}
    tbot.data["userlevels"]["root"].update(new_root_user)
    with open('data/database/data.json', 'w') as f:
        json.dump(tbot.data, f)
    tbot_send.send_message(tbot, "These files are currently stored in data/database/data.json, so remove "
                                 "them when you want")

    logging.info("Writing new root user to the data.json file")
    while True:
        if tbot.data["userlevels"]["root"][tbot.sender_id]:
            tbot_send.send_message(tbot, "Press /start to continue")
            return False
