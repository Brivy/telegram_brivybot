#!/usr/bin/env bash

echo "Enter your botID. This ID has been given by the botfather and looks like the following:
'938457311248:ABC-12ab4F_hyz_AedeahL3yz-7kfn_waGes3'"
read botID

mkdir -p ../data/database
cat << EOF > ../data/database/data.json
{
  "userlevels": {
    "root": {
    },
    "user": {
    },
    "block": {
    }
  },
  "telegram": {
    "api": "https://api.telegram.org/bot{}/",
    "token": "${botID}"
  },
  "domoticz": {
    "userinfo": {
      "username": "",
      "password": ""
    },
    "links": {
      "base": {
        "ip": "",
        "port": "",
        "home": "",
        "security": "username={}&password={}"
      },
      "command": {
        "getInfo": "&type=command&param=getversion",
        "getSunrise": "&type=command&param=getSunRiseSet",
        "logTo": "&type=command&param=addlogmessage&message={}",
        "notificationTo": "&type=command&param=sendnotification&subject={}&body={}",
        "sNotificationTo": "&type=command&param=sendnotification&subject={}&body={}&subsystem={}"
      },
      "device": {
        "getDevice": "&type=devices&rid={}",
        "getAllByType": "&type=devices&filter={}&used={}",
        "getAllByTypeSort": "&type=devices&filter={}&used={}&order={}",
        "getAllFav": "&type=devices&used={}&filter={}&favorite=1",
        "addDevice": "&type=setused&idx={}&name={}&used=true",
        "removeDevice": "&type=command&param=setunused&idx={}",
        "editDevice": "&addjvalue=0&addjvalue2=0&customimage=0&description=&idx={}&name={}&options=&protected=false&strparam1=&strparam2=&switchtype=0&type=setused&used=true"
      },
      "lights": {
        "getAllLights": "&type=command&param=getlightswitches",
        "switchLight": "&type=command&param=switchlight&idx={}&switchcmd={}",
        "switchProtLight": "&type=command&param=switchlight&idx={}&switchcmd={}&passcode={}",
        "toggleLight": "&type=command&param=switchlight&idx={}&switchcmd=Toggle"
      }
    }
  }
}
EOF

echo "File created"
