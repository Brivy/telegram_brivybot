# **Python telegram bot**
_Work in progress_

A telegram bot build with the language python.
For an installation guide, please check the header "installation"

### Description
This bot has been build to control Domoticz (website for home automation) with 
only telegram. For example, you can read, test and add devices without even
opening the Domoticz interface.
  &nbsp;

# Functionality
The bot contains a lot of functions, but not all of these functions are for
every user. With an extended user management system, the bot can handle 
multiple users at the same time.

It also runs every command in a different thread (a.k.a. multi threading).
This is required to perform every command asynchronous.


### User management
The bot manages his users with the following levels:

**Level** | **Access**
------|-------
root | Access to all the functions that the bot has to offer
user | Access to the most basic functions (like turning lights on and off)
block | No access to the bot


### Multi threading
Without threads, the bot will perform each command behind each other. If a
command takes time to complete, other users will be stuck!


### In-dept functionality
With different levels come different commands. Users and root users can both 
execute commands, but root users can also configure some settings. To be clear:
the root user can do everything the normal user can.

#### _the root user_
The root has the following options (all of these options can be found within 
the settings menu):
* Edit device data
    * Test devices
    * Add devices
    * Remove devices
* Edit user data
    * Add users to a different user level
* Edit data stored within the data.json file. This includes:
    * The telegrambot token
    * The domoticz username
    * The domoticz password
    * The domoticz IP
    * The domoticz port

#### _The normal users_
The user has some commands that the root users also can use:
* Get all the connected lights
    * Turn them on and off with commands
* Get all kinds of weather meters
    * Read the weather meters from within the app
    * Get an complete weather message every 6 hours
* Get help when needed
* Some shortcuts and stuff

# Installation
The installation on a linux server (most preferably a raspberry pi) somewhat easy.

## Preparation
First things, you need to have a running domoticz server. This won't be
covered here but you can check out [the domoticz website](https://www.domoticz.com/).

Furthermore you need to have an running python 3 (__NOT python 2.*__) environment (see [here](https://www.python.org/)
for python) and a running telegram bot (get started with the botfather within
telegram itself (documentation found [here](https://core.telegram.org/bots)). 
From this bot, you need to collect the telegrambot token (see down below).

## Step 1 - Install the bash script
From the release page, download the latest version (or use the command below). 
Place this package on a convenient place (no root access is required for this 
package). When downloaded, install the bash script with the commands below.

```bash
git clone git@gitlab.com:Brivy/telegram_brivybot.git telegrambot
cd telegrambot/install
chmod +x install.sh
./install.sh
```

Then enter the **telegrambot token** that the botfather has granted to you.
With this token, the python bot will correspond with the correct telegrambot

```
"Enter your botID. This ID has been given by the botfather and looks like the following:
938457311248:ABC-12ab4F_hyz_AedeahL3yz-7kfn_waGes3"
```

## Step 2 - Make the bot runnable and execute it
The bot needs modified to runnable. This can be done by using the following
set of commands:

```bash
cd ..
chmod +x main.py
./main.py
```

The bot is now running in the console!


## Step 3 - Initialize the bot
The bot needs some variables to work with. These can be installed within the bot!
Just open your telegram chat and navigate to the bot. If you've done it correctly
then you need to only need to message it once before it can communicate with you.

#### **The IP address**
After the welcome messages, the bot will ask for an ip address. This must be
a valid IP number or "localhost". 

Do the following when:
* If your domoticz server and your telegrambot server are not on the same device
enter your global **IP address** (make sure that you port forward the domoticz port, 
or else you can't access it from over the internet!)
* If your domoticz server and your telegrambot server are on the same device,
just enter **"localhost"** (no port forward needed)

#### **The port number**
After the IP address, the domoticz port is required. This port is initialized when
configuring domoticz and most of the time, this is port 8080. (The bot does NOT 
support https at the moment)


#### **Connection test**
After the IP address and the port are filled in, the connection to the domoticz
server will be tested. You will receive a message with the results of this test.

* If no connection could be established, than the wrong ip/port is probably used.
* If a connection is established but a username and password is required, then
you need to press "yes"
* If the connection is established, than the setup is completed!


#### **Username and password**
When you port forward 8080, everybody can find your domoticz server!
That is why it's recommended to protect it with an username and password. 
The username and password must be entered within the bot to established a connection.
This data will be stored in the project folders itself (data/database/data.json) and will not be shared.

After you have filled up the username and password, the bot will test the connection
again, but this time with the username and password. 

* If no connection could be established, than you used the wrong username/password
* Otherwise, the user has successfully connected to the server

If a wrong username/password is used, you can try again. But if you don't want to
set it up at the moment, you can skip this step (the username and password can
be filled up later in the settings menu)


#### **Finishing up**
After the domoticz configuration, a root user will be created. He can edit all
these parameters later in the settings menu.


# Contact
Questions?
Please contact my google account at: brianyperen@gmail.com