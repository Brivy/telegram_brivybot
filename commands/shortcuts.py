"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


shortcuts.py
----------------
This file contains all the commands functions
that are (or are affected by) shortcuts
"""
import logging
import re
from domoticz.hardware import lightswitch
from bot import tbot_send

def get_shortcut(tbot, command):
    """
    Build-in shortcut to turn devices on and off without the button menu

    :param tbot: The telegrambot itself
    :param command: The input of the user
    :return: Return the status code
    """
    command_parts = re.match(r'^/[Ss](\d+)', command)
    idx = command_parts.group(1)
    device = lightswitch.LightSwitch(idx)

    resp = device.is_light_switch()
    if "200" not in resp:
        return resp

    resp = device.change_status(tbot)
    if "200" not in resp:
        return resp

    logging.info("Using shortcut command: " + command)
    return "200"
