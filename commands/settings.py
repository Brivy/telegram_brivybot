"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


edit_data.py
----------------
This file contains commands to edit
settings within the bot
"""
import logging
import subprocess
from pathlib import Path
from bot.keyboard import initialize_keyboard
from bot import tbot_send


def get_settings_options(tbot):
    """
    Send a keyboard with all the filter settings to the user

    :param tbot: The telegrambot itself
    :return: Return a correct status code
    """
    keyboard = initialize_keyboard.settings_menu()
    tbot_send.send_message(tbot, "The following settings are available", keyboard)
    return "200"


def get_general_data_options(tbot):
    """
    Show the general data.json options to the user

    :param tbot: the telegrambot itself
    :return: Return a correct status code
    """
    keyboard = initialize_keyboard.general_data_option_menu()
    tbot_send.send_message(tbot, "This are the main data points to edit: ", keyboard)
    return "200"


def get_log(tbot):
    """
    Get some lines from the application.log file

    :param tbot: The telegrambot itself
    :return: Return a status code
    """
    path_to_file = "data/database/application.log"
    if not Path(path_to_file).is_file():
        logging.error("No application.log file found, did you delete the file?")
        return "404: No application.log file found, did you delete the file?"
    log_message = tail(path_to_file)
    tbot_send.send_message(tbot, log_message)
    return "200"


def tail(file):
    """
    Tail the last 10 lines of the application.log file
    This provides a quick insight in the log

    :param file: The application.log file
    :return: Return a compact message of all the logs
    """
    file_obj = subprocess.Popen(['tail', '-F', file],
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    log_message = ""
    for i in range(0, 10):
        line = file_obj.stdout.readline().decode("utf-8")
        log_message += str(line)
    return log_message
