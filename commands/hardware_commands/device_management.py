"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


device_management.py
----------------
This file contains commands to add a
device to domoticz
"""
import logging
import re
from commands.hardware_commands import general
from domoticz import initialize_domoticz
from bot.keyboard import initialize_keyboard
import requests
from bot import tbot_send, tbot_message
from commands.commands import t_data


def get_device_option_menu(tbot):
    """
    Send a keyboard with all the filter device options to the user

    :param tbot: The telegrambot itself
    :return: Return a correct status code
    """
    keyboard = initialize_keyboard.device_option_menu()
    tbot_send.send_message(tbot, "The following device options are available", keyboard)
    return "200"


def get_all_not_used(tbot, command):
    """
    Retrieve all the not used devices from domoticz

    :param tbot: The telegrambot itself
    :param command: The user input message
    :return: Return the status code of the domoticz API
    """
    try:
        command_split = re.match(r'^/device (all|used|not used)$', command)
        status = command_split.group(1)
        if status == "used":
            status = "true"
        elif status == "not used":
            status = "false"
        else:
            status = ""

        domoticz_obj = initialize_domoticz.Domoticz()
        if not domoticz_obj.test_auth_domoticz_api():
            return "401: No valid domoticz username/password"

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["getAllByType"].format("all", status)
        resp = requests.get(url)
        if re.search(r'result', str(resp.json())) is None:
            return "404: No devices found"

        keyboard = initialize_keyboard.ordered_device_menu(resp.json()["result"])
        tbot_send.send_message(tbot, "The following devices are (not yet) set up:", keyboard)
        return resp
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"


def add_device_domoticz(tbot, command, keyboard):
    """
    Add a device to the domoticz devices
        This process will happen in four steps:
        - First: Check if the idx is correct
        - Second: Check if the device is in the use of unused state
        - Third: Check if the name is empty

    :param tbot: The telegrambot itself
    :param command: The input of the user
    :param keyboard: The standard menu keyboard
    :return: Return a status code
    """
    try:
        command_split = re.match(r'[A-a]dd.* (\d{1,3}) (\w+)', command)
        idx = command_split.group(1)
        name = command_split.group(2)
        domoticz_obj = initialize_domoticz.Domoticz()
        if not domoticz_obj.test_auth_domoticz_api():
            return "401: No valid domoticz username/password"

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["getDevice"].format(idx)
        json_obj = requests.get(url).json()
        if re.search(r'result', str(json_obj)) is None:
            return "404: No valid idx given"
        elif re.search(r'\'Used\': 1', str(json_obj)) is not None:
            return "404: Can't add used device"

        # Make the user enter a device name
        if name == "to":
            while True:
                tbot_send.send_message(tbot, "Add a name to the device:")
                name = tbot_message.get_message(tbot, t_data.sender_id)
                if "409" in str(name):
                    logging.info("409 found in name, lock application")
                    return name
                if name:
                    logging.info("Lock revoked")
                    tbot.lock = False
                    break

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["addDevice"].format(idx, name)
        resp = requests.get(url)

        logging.info("The device with the name '" + name + "' has been added")
        tbot_send.send_message(tbot, "The device has been added!", keyboard)
        return resp
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"


def remove_device_domoticz(tbot, command, keyboard):
    """
    Remove the device from domoticz devices
    This process will happen in four steps:
        - First: Check if the idx is correct
        - Second: Check if the device is in the use of unused state
        - Third: the name will be set to "Unknown"
        - Four: the device will be set to unused

    :param tbot: The telegrambot itself
    :param command: The input of the user
    :param keyboard: The standard menu keyboard
    :return: Return a status code
    """
    try:
        command_split = re.match(r'^[R-r]emove.* (\d{1,3})', command)
        idx = command_split.group(1)
        domoticz_obj = initialize_domoticz.Domoticz()
        if not domoticz_obj.test_auth_domoticz_api():
            return "401: No valid domoticz username/password"

        # Start the four checks
        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["getDevice"].format(idx)
        json_obj = requests.get(url).json()
        if re.search(r'result', str(json_obj)) is None:
            return "404: No valid idx given"
        elif re.search(r'\'Used\': 0', str(json_obj)) is not None:
            return "404: Can't remove unused device"

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["editDevice"].format(idx, "Unknown")
        resp = requests.get(url)
        if re.search(r'200', str(resp)) is None:
            return resp

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["removeDevice"].format(idx)
        resp = requests.get(url)
        if re.search(r'200', str(resp)) is None:
            return resp

        logging.info("The device with the idx '" + idx + "' has been removed")
        tbot_send.send_message(tbot, "The device has been removed!", keyboard)
        return resp
    except requests.exceptions.ConnectionError:
        logging.error("Connection timed out")
        return "404: Connection timed out"


def rename_device(tbot, command):
    """
    Rename a given device by idx

    :param tbot: The telegrambot itself
    :param command: The input of the user
    :return: Return a status code
    """
    try:
        command_split = re.match(r'^[R-r]ename device (\d+)$', command)
        idx = command_split.group(1)
        domoticz_obj = initialize_domoticz.Domoticz()

        if not domoticz_obj.test_auth_domoticz_api():
            return "401: No valid domoticz username/password"

        while True:
            tbot_send.send_message(tbot, "Please enter a new name for device " + idx)
            new_name = tbot_message.get_message(tbot, t_data.sender_id)

            if new_name:
                logging.info("Lock revoked")
                tbot.lock = False
                break

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["editDevice"].format(idx, new_name)
        resp = requests.get(url)
        if re.search(r'200', str(resp)) is None:
            return resp

        logging.info("The device with the idx '" + idx + "' has been renamed to '" + new_name + "'")
        tbot_send.send_message(tbot, "The device has been renamed!")
        return resp
    except requests.exceptions.ConnectionError:
        logging.error("Connection timed out")
        return "404: Connection timed out"



def test_device(tbot, command):
    """
    Test a device before you add him to your menu
    First, the device will be made by type and idx
    Second, get the device specific options

    :param tbot: The telegrambot itself
    :param command: The input of the user
    :return: Return a status code
    """
    command_parts = re.match(r'^Test (used|unused) device (\d+) \((.+)\)$', command)
    added = False
    if command_parts.group(1) == "used":
        added = True
    idx = command_parts.group(2)
    device_type = command_parts.group(3)
    keyboard = initialize_keyboard.device_menu(idx, device_type, added)
    device = general.create_hardware_obj(device_type, idx)
    resp = general.get_device_specific_options(tbot, device, keyboard)
    logging.info("Device " + idx + " has been tested and the response is: " + str(resp))
    return resp
