"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


light_commands.py
----------------
This file contains all the commands where lights/switches
that affect light and switches
"""

from bot import tbot_send
import re
from domoticz import initialize_domoticz
import requests
from bot.keyboard import initialize_keyboard


def get_all_lights(tbot):
    """
    Retrieve all the lights of the domoticz API

    :param tbot: The telegrambot itself
    :return: Return the status code of the domoticz API
    """
    try:
        domoticz_obj = initialize_domoticz.Domoticz()
        if not domoticz_obj.test_auth_domoticz_api():
            return "401: No valid domoticz username/password"

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["getAllByType"].format("light", True)
        resp = requests.get(url)

        if re.search(r'result', str(resp.json())) is None:
            return "404 No lights found"

        keyboard = initialize_keyboard.all_lights_menu(resp.json()["result"])
        tbot_send.send_message(tbot, "The following lights are available:", keyboard)
        return resp
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"
