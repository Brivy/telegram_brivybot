"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


general.py
----------------
This file contains general commands that
belong to hardware commands
"""
import logging

import requests
from bot.keyboard import initialize_keyboard
from bot import tbot_send
from domoticz.hardware import lightswitch, temp_hum, wind, uv_index, rain, visibility, device, custom_device
from domoticz import initialize_domoticz
import domoticz
import re
import domoticz.hardware


def get_device_specific_options(tbot, device_obj, keyboard):
    """
    Searches for the specific device and assigns the right functions to him
    The following devices are compatible:
        - The (light)switch
        - The temp/hum meter
        - The wind meter
        - The uv index
        - The amount of rain


    :param tbot: The telegrambot itself
    :param device_obj: The device that is going to be controlled
    :param keyboard: The keyboard style in JSON
    :return: Return the status code
    """
    try:
        if type(device_obj) is domoticz.hardware.lightswitch.LightSwitch:
            response = device_obj.change_status(tbot, keyboard)
        else:
            response = device_obj.return_compact_data(tbot, keyboard)
        return response
    except AttributeError:
        return "404: No device method found"



def get_device_info(tbot, command):
    """
    Get the information about a device in a compact format
    This is retrieved by the domoticz API

    :param tbot: The telegrambot itself
    :param command: The user input message
    :return: Return the status code
    """
    try:
        command_parts = re.match(r'^[D-d]evice (\d+)( - \((.+)\))?$', command)
        idx = command_parts.group(1)

        domoticz_obj = initialize_domoticz.Domoticz()
        if not domoticz_obj.test_auth_domoticz_api():
            return "401: No valid domoticz username/password"

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["getDevice"].format(idx)
        json_url = requests.get(url).json()

        if re.search(r'result', str(json_url)) is None:
            return "404: idx not found"

        name = json_url["result"][0]["Name"]
        used = json_url["result"][0]["Used"]
        device_type = json_url["result"][0]["Type"]
        sub_type = json_url["result"][0]["SubType"]
        raw_data = json_url["result"][0]["Data"]
        connected_with = json_url["result"][0]["HardwareName"]
        last_update = json_url["result"][0]["LastUpdate"]
        keyboard = initialize_keyboard.device_menu(idx, device_type, used)

        if used == 0:
            used = "No"
        else:
            used = "Yes"

        tbot_send.send_message(tbot, "********************************\n"
                                     "**** Information about device " + idx + " ****\n"
                                     "********************************\n"
                                     "Name: " + name + "\n"
                                     "Used: " + used + "\n"
                                     "Device type: " + device_type + "\n"
                                     "Subtype: " + sub_type + "\n"
                                     "Raw data: " + raw_data + "\n"
                                     "Connected with: " + connected_with + "\n"
                                     "Last update: " + last_update + "\n"
                                     "********************************\n", keyboard)
        logging.info("Got device info from '" + name + "' and idx '" + idx + "'")
        return "200"
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"


def return_device(command):
    """
    When a device commando has been found, show a new keyboard menu
    First a new switch will be made (because no other devices are here yet)
    With this information combined with the domoticz API, a new menu will be created

    :param command: The user input
    :return: The device that has all the information about it
    """
    try:
        command_parts = re.match(r'^(Light|Device|Meter) (\d+) - ([\w\s]+)( \((On|Off) -> (On|Off)\))?$', command)
        idx = command_parts.group(2)
        device_name = command_parts.group(3)

        # Initialize domoticz and API
        domoticz_obj = initialize_domoticz.Domoticz()
        if not domoticz_obj.test_auth_domoticz_api():
            return "401: No valid domoticz username/password"

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["getDevice"].format(idx)
        json_url = requests.get(url).json()

        # Check if the command is correct
        device_type = check_correct_command(json_url, device_name)
        if "404" in device_type:
            return device_type

        # Return the device
        return create_hardware_obj(device_type, idx)
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"


def check_correct_command(json_url, device_name):
    """
    Check if a device lookup command is correct and
    return the device_type of the device

    The following checks are done:
        - Check if result is present in the json_url
        - Check if one of the types is present in the json_url
        - Check if the device name is the same as the user command input

    :param json_url: All the information about the device that is
    retrieved with the domoticz API and converted to json
    :param device_name: The name of the device that is retrieved from
    the user command
    :return: Return an user error OR a device device_type when no errors are found
    """
    if re.search(r'result', str(json_url)) is None:
        return "404: idx not found"
    device_type = json_url["result"][0]["Type"]
    if re.match(r'^(Light/Switch|Temp \+ Humidity \+ Baro|Wind|UV|Rain|General)$', device_type) is None:
        return "404: Type not found"
    elif json_url["result"][0]["Name"] != device_name:
        return "404: Device name is not correct"
    else:
        logging.debug("Passed all command checks")
        return device_type


def create_hardware_obj(device_type, idx):
    """
    Create a object of a specific hardware class

    :param device_type: The device_type of the object
    :param idx: The ID of the device
    :return: Returns an object of the given device_type
    """
    if device_type == "Light/Switch":
        logging.debug("Found light/switch")
        return lightswitch.LightSwitch(idx)
    elif device_type == "Temp + Humidity + Baro":
        logging.debug("Found temp/hum/baro")
        return temp_hum.TempHum(idx)
    elif device_type == "Wind":
        logging.debug("Found wind")
        return wind.Wind(idx)
    elif device_type == "UV":
        logging.debug("Found UV")
        return uv_index.UvMeter(idx)
    elif device_type == "Rain":
        logging.debug("Found rain")
        return rain.Rain(idx)
    elif device_type == "General":
        logging.debug("Found general")
        return subtype_detector(idx)
    else:
        logging.error("Didn't found this hardware type, please contact brianyperen@gmail.com")


def subtype_detector(idx):
    """
    Detect the subtype of a device

    :param idx: The ID of the device
    :return: Returns an object of the given device_type
    """
    device_subtype = device.Device(idx).json_url["result"][0]["SubType"]
    if device_subtype == 'Visibility':
        logging.debug("Found visibility subtype")
        return visibility.Visibility(idx)
    elif device_subtype == 'Custom Sensor':
        logging.debug("Found custom sensor subtype")
        return custom_device.CustomDevice(idx)
