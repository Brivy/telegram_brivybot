"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


temperature_commands.py
----------------
This file contains all the commands functions
that affect temperature
"""
import re
import requests
from bot import tbot_send
from bot.keyboard import initialize_keyboard
from domoticz import initialize_domoticz


def get_all_temp(tbot):
    """
    Retrieve all the temperature and humidity meters of the domoticz API

    :param tbot: The telegrambot itself
    :return: Return the status code of the domoticz API
    """
    try:
        domoticz_obj = initialize_domoticz.Domoticz()
        if not domoticz_obj.test_auth_domoticz_api():
            return "401: No valid domoticz username/password"

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["getAllByType"].format("temp", True)
        resp = requests.get(url)
        if re.search(r'result', str(resp.json())) is None:
            return "404: No temperature meters found"

        keyboard = initialize_keyboard.all_weather_menu(resp.json()["result"])
        tbot_send.send_message(tbot, "The following temperature meters are available:", keyboard)
        return resp
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"
