"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


utility_commands.py
----------------
This file contains all the commands functions
that affect the utility devices
"""
import logging
import re
from bot import tbot_send
from commands.hardware_commands import general
from domoticz import initialize_domoticz
import requests
from bot.keyboard import initialize_keyboard

def get_all_utility(tbot):
    """
    Retrieve all the utility devices of the domoticz API

    :param tbot: The telegrambot itself
    :return: Return the status code of the domoticz API
    """
    try:
        domoticz_obj = initialize_domoticz.Domoticz()
        if not domoticz_obj.test_auth_domoticz_api():
            return "401: No valid domoticz username/password"

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["getAllByType"].format("utility", True)
        resp = requests.get(url)
        if re.search(r'result', str(resp.json())) is None:
            return "404: No meters found"

        keyboard = initialize_keyboard.all_weather_menu(resp.json()["result"])
        tbot_send.send_message(tbot, "The following utility devices are available:", keyboard)
        return resp
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"
