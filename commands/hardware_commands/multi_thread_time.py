"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


multi_thread_time.py
---------------
This Threading class has been build to update
send a automatic message containing the weather
to the root user
"""
import logging
import re
import threading
import datetime
import time
from commands.hardware_commands import weather_commands


class MultiThreadTime (threading.Thread):


    def __init__(self, tbot, name, counter):
        """
        Initialize the timed weather info thread
        This thread needs the following parameters ot work:
            - The tbot: The telegrambot that he needs to update
            - A thread_id: This way, you can recognize the thread in the main program
            - A name: The thread must be named
            - A counter: Counts how many threads there have been

        :param tbot: The telegrambot itself
        :param name: The name of the thread
        :param counter: The ID and the counter of the thread
        """
        threading.Thread.__init__(self)
        self.tbot = tbot
        self.thread_id = counter
        self.name = name
        self.counter = counter
        logging.debug("Initialized MultiThreadTime")


    def run(self):
        """
        While this thread is running, the function will check every
        minute if a certain time has been reach
        When reached, send the weather message to the user

        The following times are configured:
            - Between 06:00:00 - 06:00:59
            - Between 12:00:00 - 12:00:59
            - Between 18:00:00 - 18:00:59

        :return: This function does not stop
        """
        logging.info("Starting thread MultiThreadTime")
        while True:
            time_now = datetime.datetime.now().time()
            if re.match(r'^(06|12|18):00:[0-5][0-9]', str(time_now)) is not None:
                logging.info("Sending weather message")
                weather_commands.get_complete_weather_message(self.tbot)
            time.sleep(60)
