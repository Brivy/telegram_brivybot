"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


weather_commands.py
----------------
This file contains all the commands functions
that affect the weather
"""
import logging
import re
from bot import tbot_send
from commands.hardware_commands import general
from domoticz import initialize_domoticz
import requests
from bot.keyboard import initialize_keyboard


def get_all_weather(tbot):
    """
    Retrieve all the weather meters of the domoticz API

    :param tbot: The telegrambot itself
    :return: Return the status code of the domoticz API
    """
    try:
        domoticz_obj = initialize_domoticz.Domoticz()
        if not domoticz_obj.test_auth_domoticz_api():
            return "401: No valid domoticz username/password"

        url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["getAllByType"].format("weather", True)
        resp = requests.get(url)
        if re.search(r'result', str(resp.json())) is None:
            return "404: No meters found"

        keyboard = initialize_keyboard.all_weather_menu(resp.json()["result"])
        tbot_send.send_message(tbot, "The following weather meters are available:", keyboard)
        return resp
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"


def check_duplicate_weather(data, already_used_device):
    """
    Check if a type of a device already is used in the
    complete weather message

    :param data: The device his data
    :param already_used_device: The array with the already used types
    :return: True if duplicate found, False if not
    """
    for types in already_used_device:
        if data["Type"] == types:
            return True
    return False


def get_complete_weather_message(tbot):
    """
    Construct a global weather message with the data from all weather devices
    The following devices are supported:
        - Temperature/Humidity/Baro
        - Wind
        - UV
        - Rain
        - Visibility

    :param tbot: The telegrambot itself
    :return: Return the status code
    """
    all_device_data = {}
    domoticz_obj = initialize_domoticz.Domoticz()
    if not domoticz_obj.test_auth_domoticz_api():
        return "401: No valid domoticz username/password"

    url = domoticz_obj.home_url + domoticz_obj.dom_links["device"]["getAllByType"].format("weather", True)
    resp = requests.get(url)
    if re.search(r'result', str(resp.json())) is None:
        return "404: No devices found"

    result_array = resp.json()["result"]
    already_used_device = []

    # Filter through all the weather devices
    for data in result_array:
        if check_duplicate_weather(data, already_used_device):
            continue

        device_list = []
        device_type = data["Type"]
        device_idx = data["idx"]
        device_data = data["Data"]
        device = general.create_hardware_obj(device_type, device_idx)

        # Get the information
        for data_regex in re.findall(device.regex_pattern, str(device_data)):
            if re.match(r'^\(', str(data_regex)) is None:
                device_list.append(data_regex)
            else:
                for item in data_regex:
                    device_list.append(item)

        # Extra parameters
        if device_type == "Temp + Humidity + Baro":
            device_list.append(device.get_last_update())
            device_list.append(device.get_forecast())
            device_list.append(device.get_humidity_status())

        # Create dictionary
        all_device_data[device_type] = device_list
        already_used_device.append(data["Type"])

    # Construct and send the message
    message = construct_global_weather(all_device_data)
    tbot_send.contact_root_message(tbot, message)

    logging.info("Constructed global weather message and send it to the root user")
    return "200"


def construct_global_weather(all_device_data):
    """
    Construct a global weather message
    It will retrieve the following weather meters:
        - Temperature/Humidity/Baro
        - Wind
        - UV
        - Rain
        - Visibility

    :param all_device_data: Array of all the devices that will be added to the weather message
    :return: Return the constructed message
    """
    message = "This is the local weather:\n" \
              "********************************\n\n"

    for device_type, data in all_device_data.items():
        if device_type == "Temp + Humidity + Baro" and data != []:
            message += "**** Temperature ****\n" \
                       "Temperature: " + data[0] + "\n" \
                       "Humidity: " + data[1] + "\n" \
                       "Air pressure: " + data[2] + "\n\n\n" \
                       "************* Forecast *************\n" \
                       "This is the forecast of today: " + data[4] + "\n" \
                       "This is the status of the humidity: " + data[5] + "\n\n\n"
        elif device_type == "Wind" and data != []:
            speed = data[2] + "." + data[3]
            gust = data[4] + "." + data[5]
            message += "******* Wind *******\n" \
                       "Windspeed: " + speed + " m/s\n" \
                       "Gustspeed: " + gust + "m/s\n" \
                       "Wind direction: " + data[1] + " (" + data[0] + u"\u00b0)\n\n\n"
        elif device_type == "UV" and data != []:
            message += "******* UV index *******\n" \
                       "Current UV index: " + data[0] + " UVI\n\n\n"
        elif device_type == "Rain" and data != []:
            message += "***** Rain-o-meter *****\n" \
                       "Number of rain drops: " + data[0] + "\n" \
                       "Rate of the rain: " + data[1] + "\n\n\n"
        elif device_type == "General" and data != []:
            message += "**** Visibility ****\n" \
                       "Visibility: " + data[0] + "\n\n\n"

    return message
