"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


user_commands.py
----------------
This file contains all the commands that every
user can send to the bot
"""
import logging
import re
import time
import requests
from bot import tbot_send
from bot.keyboard import initialize_keyboard
from commands import shortcuts
from commands.hardware_commands import light_commands, weather_commands, general, temperature_commands, utility_commands
from domoticz.hardware import device
from commands.commands import t_data


def initialize_commands(tbot, command, keyboard):
    """
    When no root command has been found, initialize the user commands
    This function will try to match a command with input of the user
    When a match has been found, the bot will execute the corresponding function

    :param tbot: The telegrambot itself
    :param command: The input of the user
    :param keyboard: The standard menu keyboard
    :return: Return the response of a command, but return "None" if no suitable commands have been found
    """
    response = basic_commands(tbot, command, keyboard)
    if re.match(r'^(Light|Device|Meter) (\d+) - ([\w\s]+)( \((On|Off) -> (On|Off)\))?$', command) is not None:
        response = general.return_device(command)
        logging.info("Found response: " + str(response))
        if isinstance(response, device.Device):
            response = general.get_device_specific_options(tbot, response, keyboard)
    elif re.match(r'^((/)?([Ss])(\d+))$', command) is not None:
        response = shortcuts.get_shortcut(tbot, command)
    elif re.match(r'^/?[R-r]eturn( to )?(.+)?$', command) is not None:
        response = return_menu(tbot, command, keyboard)
    return response


def basic_commands(tbot, command, keyboard):
    """
    Initialise the basic commands for the user
    With these commands, the user can:
        - Start the bot
        - Get all the available lights
        - Get all the available meters
        - Return a help screen

    :param tbot: The telegrambot itself
    :param command: The input of the user
    :param keyboard: The standard menu keyboard
    :return: Return the response of a command, but return "None" if no suitable commands have been found
    """
    response = None
    if command == "/start":
        response = start_command(tbot, keyboard)
    elif command == "/getAllLights":
        response = light_commands.get_all_lights(tbot)
    elif command == "/getAllWeather":
        response = weather_commands.get_all_weather(tbot)
    elif command == "/getAllTemp":
        response = temperature_commands.get_all_temp(tbot)
    elif command == "/getAllUtility":
        response = utility_commands.get_all_utility(tbot)
    elif command == "/help":
        response = help_command(tbot)
    elif command == "/threading":
        response = long_test_command(tbot)

    return response


def start_command(tbot, keyboard):
    """
    Start the bot by sending a welcome text and image
    (Mostly used for testing a new user startup)

    :param tbot: The telegrambot itself
    :param keyboard: The standard menu keyboard
    :return: This function does not return anything
    """
    try:
        api_url = "https://api.telegram.org/bot{}/".format(tbot.token)
        method = "sendPhoto"
        params = {"chat_id": t_data.sender_id,
                  "photo": "https://i.imgur.com/li8FgQt.png"}
        resp = requests.get(api_url + method, params)

        tbot_send.send_message(tbot, "Welcome to the domoticz bot server\n"
                                     "For help press the /help command, otherwise use the keyboard down below\n\n"
                                     "For questions about this bot, please contact brianyperen@gmail.com\n\n"
                                     "Enjoy your stay!", keyboard)
        return resp
    except requests.exceptions.ConnectionError:
        return "404: Connection timed out"


def help_command(tbot):
    """
    Sends a help text message to the user

    :param tbot: The telegrambot itself
    :return: This function does not return anything
    """
    tbot_send.send_message(tbot, "This is your help screen:\n"
                                 "*****************************************\n"
                                 "The following commands are available to you:\n"
                                 "/help - Used to help the user\n"
                                 "\n"
                                 "/getAllLights - Used to get all device categories\n"
                                 "\n"
                                 "/getAllTemp - Used to get all the temperature meters\n"
                                 "\n"
                                 "/s{id} - Shortcut to turn lights on and off by ID\n"
                                 "Replace the {id} with the idx to turn that light on/off\n"
                                 "\n"
                                 "/return - Used to return to the main menu" 
                                 "*****************************************")
    return "200"


def return_menu(tbot, command, keyboard):
    """
    Return to different menu's when using different return commands

    :param tbot: The telegrambot itself
    :param command: The user input
    :param keyboard: The standard menu keyboard
    :return: Return a status code
    """
    command_parts = re.match(r'^/?[R-r]eturn( to the )?(.+)?$', command)
    menu = command_parts.group(2)

    if menu == "devices menu":
        keyboard = initialize_keyboard.device_option_menu()
        tbot_send.send_message(tbot, "Return to the devices menu..", keyboard)
    elif menu == "users menu":
        keyboard = initialize_keyboard.display_users_menu()
        tbot_send.send_message(tbot, "Return to the users menu..", keyboard)
    elif menu == "settings menu":
        keyboard = initialize_keyboard.settings_menu()
        tbot_send.send_message(tbot, "Return to the settings menu..", keyboard)
    elif menu == "data options menu":
        keyboard = initialize_keyboard.general_data_option_menu()
        tbot_send.send_message(tbot, "Return to the data options menu..", keyboard)
    else:
        tbot_send.send_message(tbot, "Return to the main menu..", keyboard)
    return "200"


def long_test_command(tbot):
    """
    Test multi threading

    :param tbot: The telegrambot itself
    :return: Returns a status code
    """
    tbot_send.send_message(tbot, "testing threading")
    time.sleep(10)
    tbot_send.send_message(tbot, "halfway testing threading")
    time.sleep(10)
    tbot_send.send_message(tbot, "stopped testing threading")
    return "200"
