"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


catch_response.py
----------------
This file catches the response codes that commands
send to the bot
Most of the time the responses are "200" and nothing will happen
With other responses, the bot sends a error message to the user
"""
import logging
import re
from bot import tbot_send


def return_response(tbot, response, keyboard):
    """
    Catch the status code and return a message to the user
    when no "200" response has been found

    :param tbot: The telegrambot itself
    :param response: The returning response code
    :param keyboard: The standard menu keyboard
    :return: Return the response of a command, but return "None" if no suitable commands have been found
    """
    if response is None:
        logging.warning("No valid command (response: None)")
        tbot_send.send_message(tbot, "No valid command", keyboard)
    elif re.match(r'^((.+)? ?(400) ?(.+)?)$', str(response)) is not None:
        logging.warning("Something does not respond (response: " + str(response)+ ")")
        tbot_send.send_message(tbot, "Something does not respond (error 400)", keyboard)
    elif re.match(r'^((.+)? ?(409) ?(.+)?)$', str(response)) is not None:
        logging.info("409 notice: " + response)
        return
    elif re.match(r'^((.+)? ?(200) ?(.+)?)$', str(response)) is None:
        logging.warning("Error code " + response)
        tbot_send.send_message(tbot, "Error code " + str(response), keyboard)
