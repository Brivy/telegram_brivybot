"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


commands.py
-----------
This file handles the commands that are send by the user
Most of the commands are only accessible by the
root user, but normal users can also sends commands to this file
"""
import logging
import threading
# Start a TLS (Thread local storage)
# Used to hold the specific sender name and id
from bot import tbot_send

t_data = threading.local()
from bot.keyboard import initialize_keyboard
from commands import root_commands, user_commands, catch_response


class CommandThreading (threading.Thread):

    def __init__(self, tbot, name, counter, command):
        """
        Initialize the command thread
        This thread needs the following parameters ot work:
            - The tbot: The telegrambot that he needs to update
            - A thread_id: This way, you can recognize the thread in the main program
            - A name: The thread must be named
            - A counter: Counts how many threads there have been
            - A command: The command that the bot needs to execute

        :param tbot: The telegrambot itself
        :param name: The name of the thread
        :param counter: The ID and the counter of the thread
        :param command: The user input
        """
        threading.Thread.__init__(self)
        self.tbot = tbot
        self.thread_id = counter
        self.name = name
        self.counter = counter
        self.command = command
        logging.debug("Initialized CommandThreading thread")


    def run(self):
        """
        Run commands multi-threaded!
        All commands will have their own thread implemented
        if not, heavy tasks will slow down pending tbot commands

        The TLS will also be initialized within this function
            - Sender_id = the id from the user that sends the command
            - Sender_name = the name from the user that sends the command

        :return: Returns the thread when the command has been executed
        """
        try:
            logging.debug("Starting thread CommandThreading")
            t_data.sender_id = self.tbot.sender_id
            t_data.sender_name = self.tbot.sender_name

            root_check = check_root(self.tbot)
            get_command(self.command, self.tbot, root_check)
        except KeyboardInterrupt:
            logging.error("Threading stopped by keyboard interrupt")
            raise
        # except Exception as e:
        #     logging.error(e)
        #     tbot_send.send_message(self.tbot, "Error 999: Something went terribly wrong within your thread,"
        #                                       " see the logs for more info)")


def check_root(tbot):
    """
    Implemented because the startup_menu and the bot_menu both need to check for root

    :param tbot: The telegrambot itself
    :return: True if a root user has been found and False otherwise
    """
    root_users = tbot.data["userlevels"]["root"]
    for user_id in root_users:
        if int(user_id) == t_data.sender_id:
            return True
    return False


def get_command(command, tbot, root_check=False):
    """
    This function first checks for a root user
    When no has been found, continue with the user only commands
    If no suitable user commands has been found, return an error message

    :param command: The command itself
    :param tbot: The telegram bot itself
    :param root_check: True if the user is Root, false if otherwise
    :return: This function does not return anything
    """
    keyboard = initialize_keyboard.bot_menu_keyboard(root_check)
    response = None

    # Check if the command has a valid type
    if not command:
        response = "404: No valid type"

    # Check for root and root commands first
    if root_check and not response:
        logging.debug("Checking for root command")
        response = root_commands.initialize_commands(tbot, command, keyboard)

    # Check for user commands when no root response has been found
    if not response:
        logging.debug("Checking for user command")
        response = user_commands.initialize_commands(tbot, command, keyboard)

    # Check if the response is an other number than 200 (error)
    catch_response.return_response(tbot, response, keyboard)
