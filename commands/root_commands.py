"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


root_commands.py
----------------
This file contains all the commands that a
root user can send
Root users have the privileged to also access
the commands within "user_commands.py"
"""
from bot import tbot_send
from commands import settings
from commands.hardware_commands import weather_commands, general, device_management
from commands.user_management import user, data_options
import re

def initialize_commands(tbot, command, keyboard):
    """
    This function will try to match a command with input of the root user
    When a match has been found, the bot will execute the corresponding function

    The order of the matches matter!

    :param tbot: The telegrambot itself
    :param command: The input of the user
    :param keyboard: The standard menu keyboard
    :return: Return the response of a command, but return "None" if no suitable commands have been found
    """
    response = None
    if command == "/settings":
        response = settings.get_settings_options(tbot)
    elif command == "/help":
        response = root_help_command(tbot)
    elif re.match(r'^([R-r]emove|[A-a]dd).* (\d{1,3})', command) is not None:
        response = device_management_options(tbot, command, keyboard)
    elif re.search(r'[D-d]evice|[M-m]eter', command) is not None:
        response = device_options(tbot, command)
    elif re.search(r'(domoticz|telegram) (\w+)', command) is not None:
        response = specific_data_options(tbot, command)
    elif re.search(r'[U-u]ser', command) is not None:
        response = user_management_options(tbot, command, keyboard)
    elif re.search(r'[E-e]dit_data', command) is not None:
        response = settings.get_general_data_options(tbot)
    elif re.match(r'^[T-t]est$', command) is not None:
        response = weather_commands.get_complete_weather_message(tbot)
    elif re.match(r'^/log$', command) is not None:
        response = settings.get_log(tbot)
    return response


def device_options(tbot, command):
    """
    Options specifically designed for device commands
    This function will try to match the given device command and return it's response

    :param tbot: The telegrambot itself
    :param command: The input of the user
    :return: Return the response of one of these commands OR None when no matches are found
    """
    response = None
    if command == "/device":
        response = device_management.get_device_option_menu(tbot)
    elif re.match(r'^[D-d]evice (\d+)( - \((.+)\))?$', command) is not None:
        response = general.get_device_info(tbot, command)
    elif re.match(r'^(/[D-d]evice) (all|used|not used)$', command) is not None:
        response = device_management.get_all_not_used(tbot, command)
    elif re.match(r'^Test (used|unused) device (\d+) \((.+)\)$', command) is not None:
        response = device_management.test_device(tbot, command)
    elif re.match(r'^[R-r]ename device (\d+)$', command) is not None:
        response = device_management.rename_device(tbot, command)

    return response


def device_management_options(tbot, command, keyboard):
    """
    Options specifically designed for management in devices
    This function will try to match the given device commands and return it's response

    :param tbot: The telegrambot itself
    :param command: The input of the user
    :param keyboard: The standard menu keyboard
    :return: Return the response of one of these commands OR None when no match is found
    """
    response = None
    if re.match(r'^([R-r]emove).* (\d{1,3})', command) is not None:
        response = device_management.remove_device_domoticz(tbot, command, keyboard)
    elif re.match(r'^([A-a]dd).* (\d{1,3}) (\w+)', command) is not None:
        response = device_management.add_device_domoticz(tbot, command, keyboard)

    return response


def user_management_options(tbot, command, keyboard):
    """
    Options specifically designed to manage users
    This function will try to match the given device commands and return it's response

    :param tbot: The telegrambot itself
    :param command: The input from the user
    :param keyboard: The standard menu keyboard
    :return: Return the response of one of these commands OR None when no match is found
    """
    response = None
    if command == "/users":
        response = user.show_user_menu(tbot)
    elif re.match(r'^[U-u]ser: (\w+) - (\d+) \((\w+)\)$', command) is not None:
        response = user.show_user_info(tbot, command)
    elif re.match(r'^(/[U-u]sers) (all|root|user|block)$', command) is not None:
        response = user.show_users_by_level(tbot, command)
    elif re.match(r'^(\w+) \((\d+)\) from \'(root|user|block)\' to '
                  r'\'(root|user|block)\' level\?$', command) is not None:
        response = user.change_user_level(tbot, command, keyboard)
    return response


def specific_data_options(tbot, command):
    """
    Options specifically designed to manage the data.json file
    This function will try to match the given device commands and return it's response

    :param tbot: The telegrambot itself
    :param command: The input from the user
    :return: Return the response of one of these commands OR None when no match is found
    """
    response = None
    if re.match(r'^The (domoticz|telegram) (\w+)$', command) is not None:
        response = data_options.show_data_specifics(tbot, command)
    elif re.match(r'^Change the (telegram|domoticz) (token|username|password|ip|port) object$', command):
        response = data_options.edit_data_object(tbot, command)
    return response


def root_help_command(tbot):
    """
    Sends a help text message to the root user

    :param tbot: The telegrambot itself
    :return: This function does not return anything
    """
    tbot_send.send_message(tbot, "This is your help screen:\n"
                                 "*****************************************\n"
                                 
                                 "Root specific commands:\n"
                                 "/settings - Edit settings of the bot\n"
                                 "\n"
                                 "/device (all|used|not used) - Get all devices of that category\n"
                                 "\n"
                                 "/users (all|root|user|block) - Get all users of that category\n"
                                 "\n"
                                 "/edit_data - Get a menu to edit specific data from the data.json file\n"
                                 "\n"
                                 "/log - Tail the application.log file"
                                 "\n"
                                 
                                 "The following commands are available to all users:\n"
                                 "/help - Used to help the user\n"
                                 "\n"
                                 "/getAllLights - Used to get all device categories\n"
                                 "\n"
                                 "/getAllTemp - Used to get all the temperature meters\n"
                                 "\n"
                                 "/s{id} - Shortcut to turn lights on and off by ID\n"
                                 "Replace the {id} with the idx to turn that light on/off\n"
                                 "\n"
                                 "/return - Used to return to the main menu" 
                                 "*****************************************")
    return "200"
