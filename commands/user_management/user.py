"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


user.py
----------------
This file contains commands to add/edit
users in the data.json file
"""
import json
import re
import logging
from bot import tbot_send
from bot.keyboard import initialize_keyboard


def show_user_menu(tbot):
    """
    Shows a keyboard layout with all the different user levels

    :param tbot: The telegrambot itself
    :return: The status code
    """
    keyboard = initialize_keyboard.display_users_menu()
    tbot_send.send_message(tbot, "The following userlevels are available:", keyboard)
    return "200"


def show_users_by_level(tbot, command):
    """
    Shows the users and their corresponding levels
    If the user chooses to show all the users, then use a different way to
    get the users

    :param tbot: The telegrambot itself
    :param command: The message from the user
    :return: The status code
    """
    user_level = re.match(r'^/[U-u]sers (all|root|user|block)$', command)
    level = user_level.group(1)
    if level == "all":
        all_user_array = tbot.data["userlevels"]
    else:
        all_user_array = tbot.data["userlevels"][level]
    keyboard = initialize_keyboard.display_users(all_user_array, level)
    tbot_send.send_message(tbot, "This are the current users:", keyboard)
    return "200"


def show_user_info(tbot, command):
    """
    Shows a menu that can up-/downgrade an user

    :param tbot: The telegrambot itself
    :param command: The message from the user
    :return: The status code
    """
    user_data = re.match(r'^[U-u]ser: (\w+) - (\d+) \((\w+)\)$', command)
    user_name = user_data.group(1)
    user_id = user_data.group(2)
    level = user_data.group(3)
    keyboard = initialize_keyboard.display_user_options(user_name, user_id, level)
    tbot_send.send_message(tbot, "What do you want to do with user " + user_name + " (id: " + user_id + ")?", keyboard)
    return "200"


def change_user_level(tbot, command, keyboard):
    """
    Change user levels based on given commands

    :param tbot: The telegrambot itself
    :param command: The message from the user
    :param keyboard: A JSON format of the home menu keyboard
    :return:
    """
    command_part = re.match(r'^(\w+) \((\d+)\) from \'(root|user|block)\' to \'(root|user|block)\' level\?$', command)
    user_name = command_part.group(1)
    user_id = command_part.group(2)
    cur_level = command_part.group(3)
    new_level = command_part.group(4)
    new_data = {user_id: user_name}
    regex_id = re.escape(user_id)

    # Check if the name is presented in his current level
    if re.search(regex_id, str(tbot.data["userlevels"][cur_level])) is None:
        return "404: No user found in the current level"

    tbot.data["userlevels"][new_level].update(new_data)
    tbot.data["userlevels"][cur_level].pop(user_id)
    logging.info("User updated to a new level")

    with open('data/database/data.json', 'w') as f:
        json.dump(tbot.data, f)

    tbot_send.send_message(tbot, "User: " + user_name + " added to level '" + new_level + "'!\n"
                                 "Please wait around 30 seconds before the user is fully updated", keyboard)
    return "200"
