"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


data_options.py
----------------
This file contains commands to data (the
data.json file)
"""
import base64
import json
import logging
import re
from domoticz import domoticz_new_install
from bot import tbot_send, tbot_message
from bot.keyboard import initialize_keyboard
from commands.commands import t_data


def show_data_specifics(tbot, command):
    """
    Return data from the data.json file with a specific key

    :param tbot: The telegrambot itself
    :param command: The user input message
    :return: The correct status code
    """
    command_parts = re.match(r'^The (domoticz|telegram) (\w+)$', command)
    data_type = command_parts.group(1)
    data_object = command_parts.group(2)
    keyboard = initialize_keyboard.edit_general_data_menu(data_type, data_object)

    if data_type == "telegram":
        specific_data = tbot.data[data_type][data_object]
    elif data_object == "username" or data_object == "password":
        encoded_data = tbot.data[data_type]["userinfo"][data_object]
        specific_data = base64.b64decode(encoded_data).decode("utf-8")
    else:
        specific_data = tbot.data[data_type]["links"]["base"][data_object]
    tbot_send.send_message(tbot, "This is the " + data_object + " data from the data.json file:\n"
                           + str(specific_data), keyboard)
    return "200"


def edit_data_object(tbot, command):
    """
    Edit specific data from the data.json file
    This way, you can manage the data from the bot within the bot

    :param tbot: The telegrambot itself
    :param command: The user input message
    :return: The correct status code
    """
    command_parts = re.match(r'^Change the (telegram|domoticz) (token|username|password|ip|port) object$', command)
    data_type = command_parts.group(1)
    data_object = command_parts.group(2)
    keyboard = initialize_keyboard.general_data_option_menu()

    tbot_send.send_message(tbot, "Enter a new " + data_type + " " + data_object)

    while True:
        new_data_value = tbot_message.get_message(tbot, t_data.sender_id)
        if check_data_type_errors(tbot, data_object, new_data_value):
            logging.info("Found correct to change within the data.json file")
            break

    new_data_json = {data_object: new_data_value}
    update_data_by_object(tbot, data_type, data_object, new_data_value, new_data_json, keyboard)
    tbot_send.send_message(tbot, "Value '" + data_object + "' changed to '" + new_data_value + "'", keyboard)
    logging.info("Writing new values to data.json")

    with open('data/database/data.json', 'w') as f:
        json.dump(tbot.data, f)

    test_new_settings(tbot, data_object)
    tbot.lock = False
    return "200"


def check_data_type_errors(tbot, data_object, new_data_value):
    """
    Run a check on specific data objects
    Not all data_objects are the same and some require standard formatting

    :param tbot: The telegrambot itself
    :param data_object: The data object type
    :param new_data_value: The new value for the data object
    :return: True is there are no problems and False otherwise
    """
    if data_object == "token":
        if re.match(r'^(\d+):([A-Za-z0-9_-]+)$', new_data_value) is None:
            tbot_send.send_message(tbot, "Please enter a valid telegram token")
            return False
    elif data_object == "ip":
        if re.match(r'^((\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})|(localhost))$', new_data_value) is None:
            tbot_send.send_message(tbot, "Please enter a valid domoticz IP address")
            return False
    elif data_object == "port":
        if re.match(r'^([0-9]|[1-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9][0-9][0-9]|[1-6][0-9][0-9][0-9][0-5])$',
                    new_data_value) is None:
            tbot_send.send_message(tbot, "Please enter a valid domoticz port")
            return False
    return True


def update_data_by_object(tbot, data_type, data_object, new_data_value, new_data_json, keyboard):
    """
    Update a data value by the data object
    This method can process the following attributes:
        - token
        - username
        - password
        - ip
        - port

    :param tbot: The telegrambot itself
    :param data_type: the type of the data object
    :param data_object: the data object himself
    :param new_data_value: the new data value for the data object
    :param new_data_json: the new dat converted into json
    :param keyboard: A json formatted keyboard
    :return: No returns
    """
    if data_type == "telegram":
        tbot.data[data_type].update(new_data_json)
    elif data_object == "username" or data_object == "password":
        base_encode_data(tbot, data_object, new_data_value)
    else:
        if data_object == "ip":
            port = tbot.data[data_type]["links"]["base"]["port"]
            new_home_value = "http://" + new_data_value + ":" + port + "/json.htm?"
        else:
            ip = tbot.data[data_type]["links"]["base"]["ip"]
            new_home_value = "http://" + ip + ":" + new_data_value + "/json.htm?"

        new_home_json = {"home": new_home_value}
        tbot.data[data_type]["links"]["base"].update(new_data_json)
        tbot.data[data_type]["links"]["base"].update(new_home_json)
        tbot_send.send_message(tbot, "Value 'home' changed to '" + new_home_value + "'", keyboard)


def base_encode_data(tbot, data_object, new_data_value):
    """
    Base64 encode the username or the password
    Domoticz will check for access over the url,
    so the data must be readable from here (that is where base64 comes in)

    This is not an encryption method!

    :param tbot: The telegrambot itself
    :param data_object: the data object himself
    :param new_data_value: the new data value for the data object
    :return: No returns
    """
    encode_data = str(base64.b64encode(bytes(new_data_value, 'ascii')).decode("utf-8"))
    domoticz_data = {data_object: encode_data}
    tbot.data["domoticz"]["userinfo"].update(domoticz_data)


def test_new_settings(tbot, data_object):
    """
    Test the new settings of the telegrambot

    :param tbot: The telegrambot itself
    :param data_object: the data object himself
    :return: No returns
    """
    logging.info("Testing new settings")
    if data_object == "ip" or data_object == "port":
        domoticz_new_install.DomoticzNewInstall(tbot).test_domoticz_url(tbot)
    elif data_object == "username" or data_object == "password":
        domoticz_new_install.DomoticzNewInstall(tbot).test_domoticz_login(tbot)