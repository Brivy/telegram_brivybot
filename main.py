#! /usr/bin/env python
"""
________       _____
___  __ )_________(_)__   ______  __
__  __  |_  ___/_  /__ | / /_  / / /
_  /_/ /_  /   _  / __ |/ /_  /_/ /
/_____/ /_/    /_/  _____/ _\__, /
                           /____/
@Brivy2018


main.py
-------
This is the main program of the bot
Here, the telegrambot class will be initialized
and the menu's will be activated
"""
import sys
# The program can't run on python 2.*
if sys.version_info[0] < 3:
    raise Exception("Must be using Python 3")

import logging
from bot import telegrambot
from home import menu
import json
from install import new_install
from pathlib import Path

def main():
    """
    First open the datasource and collect the bot his token
    After that, you can initialize the telegram bot itself

    Than the bot will check if this is a new install or an restart of the system
    When it is a new install, fill the data.json file

    Then the main will split in two loops:
        - First loop: user has to press /start to continue
        - Second loop: users can send commands to the bot

    In the second loop, users will be identified by their ID.
    Some users have root status and can send more commands that normal users
    For more information about the commands and functions, please check the documentation

    :return: The main function does not return anything
    """
    logging.basicConfig(
        filename='data/database/application.log',
        level=logging.INFO,
        format='%(levelname)s %(asctime)s %(module)s - %(funcName)s: %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S")
    logging.info("\n\n-----------------------------------Application started-----------------------------------\n\n")

    data_file = Path("data/database/data.json")
    if not data_file.is_file():
        logging.error("No data.json file found, please run a './test.sh' in the install folder")
        raise Exception("No data.json file found, please run a './test.sh' in the install folder")

    with open('data/database/data.json') as f:
        data = json.load(f)
    logging.info("Initializing the telegrambot")
    token = data["telegram"]["token"]
    tbot = telegrambot.Tbot(token)
    new_install.check_for_new_install(tbot)
    logging.info("Starting start menu")
    menu.startup_menu(tbot)
    logging.info("Starting bot menu")
    while True:
        menu.bot_menu(tbot)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        logging.error("Bot stopped by keyboard interrupt")
    except:
        raise
